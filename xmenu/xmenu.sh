#!/bin/sh
# vim:noexpandtab:

COLORSCRIPTSDIR=$HOME/Programs/color-scripts/color-scripts
CONFIGDIR=$HOME/dotfiles/dotfiles/config/


cat <<EOF | xmenu -i -p 0x25:1 | sh &
 Applications
	 Firefox	firefox
	 Terminal	st
	 Files	thunar
	 Comms
		Discord (GUI)		discord
		Discord (cordless)		st -e cordless
		 Email (Neomutt)		st -e neomutt
		 IRC (weechat)		st -e weechat
		 SMS (kde-connect)		kdeconnect-sms --style gtk2
		 Slack 	slack
		 Signal (GUI)		signal-desktop
		 Telegram (GUI)		telegram-desktop
		 Telegram (nctelegram)		nctelegram
	 Finance
		 Crypto (TUI)		st -e cointop
		 Stocks (TUI)		st -e mop
		 Stonks (CLI)		st -e mop
	 Utilities
		 Calculator (TUI)		st -e qalq
		 Calculator (GUI)		qalculate-gtk
		 Calendar (khal)		st -e khal interactive
		 Color Picker		gpick
		 Map (TUI)		st -e mapscii

		 System
			 Keyboard (Corsair)		ckb-next
			 Fonts 	gucharmap
			 Smartphone (kdeconnect)		kdeconnect-app --style gtk2
			 Docker (lazydocker)		st -e lazydocker
			 Kill Window	xkill
			 Screen Recording (OBS)		obs
			 Screenshot
				GUI	flameshot gui
				All Displays	flameshot screen -d 5000 -n 3 -p $HOME/Photos/screenshots
				Middle Display	flameshot screen -d 5000 -n 0 -p $HOME/Photos/screenshots
				Right Display	flameshot screen -d 5000 -n 1 -p $HOME/Photos/screenshots
				Left Display	flameshot screen -d 5000 -n 2 -p $HOME/Photos/screenshots
		 Personal
			 Nextcloud		nextcloud --style gtk2
			 Passwords (keepassxc)		keepassxc --style gtk2
			 Keys (Seahorse)		seahorse
		 Theming
			lxappearance 	lxappearance
			GTK (oomox) 	oomox-gui
			Qt (qt5ct) 	qt5ct --style gtk2
			WPGTK 	wpg
		 Monitors
			System (ytop)		st -e ytop
			System (bashtop)		st -e bashtop
			System (glances)		st -e glances

			Disk Usage (GUI)		baobab
			Disk Usage (TUI)		st -e ncdu
			IO (iotop)		st -e iotop

			Kernel (kmon)		st -e kmon
			Nvidia GPU (nvtop)		st -e nvtop
			Power (powertop)		st -e powertop

			DNS (dnstop)		st -e powertop
			Network Usage (jnettop)		st -e jnettop
			Network Load (nload)		st -e nload
			Bandwidth (bmon)		st -e bmon
			Media Server		st -e jellyfinips.sh
		 Media
			 EasyTag		easytag
	 Entertainment
		 Media
			 Podcasts (castero)	castero
			 RSS (newsboat)	newsboat
			 Reddit (tuir)	tuir
			 Music (cmus)	cmus
			 Spotify (GUI)	spotify
			 Spotify (spotifytui)	spt
			Soulseek (Nicotine+)	nicotine
		 Games
			 Steam	steam
			Itch	itch
			Lutris	lutris
			Tetris	tetris
			Solitaire	ttysolitaire
			Battleship	bs
			Minecraft	minecraft-launcher
			Dopewars	dopewars
		 Misc
			Color Scripts
				alpha	st -e sh -c '$COLORSCRIPTSDIR/alpha; read'
				arch	st -e sh -c '$COLORSCRIPTSDIR/arch; read'
				bars	st -e sh -c '$COLORSCRIPTSDIR/bars; read'
				blocks2	st -e sh -c '$COLORSCRIPTSDIR/blocks2; read'
				bloks	st -e sh -c '$COLORSCRIPTSDIR/bloks; read'
				colorbars	st -e sh -c '$COLORSCRIPTSDIR/colorbars; read'
				colortest	st -e sh -c '$COLORSCRIPTSDIR/colortest; read'
				colortest-slim	st -e sh -c '$COLORSCRIPTSDIR/colortest-slim; colortest'
				colorview	st -e sh -c '$COLORSCRIPTSDIR/colorview; read'
				crunch	st -e sh -c '$COLORSCRIPTSDIR/crunch; read'
				crunchbang	st -e sh -c '$COLORSCRIPTSDIR/crunchbang; read'
				crunchbang-mini	st -e sh -c '$COLORSCRIPTSDIR/crunchbang-mini; read'
				darthvader	st -e sh -c '$COLORSCRIPTSDIR/darthvader; read'
				dna	st -e sh -c '$COLORSCRIPTSDIR/dna; read'
				dotx	st -e sh -c '$COLORSCRIPTSDIR/dna; read'
				elfman	st -e sh -c '$COLORSCRIPTSDIR/elfman; read'
				faces	st -e sh -c '$COLORSCRIPTSDIR/faces; read'
				fade	st -e sh -c '$COLORSCRIPTSDIR/fade; read'
				ghosts	st -e sh -c '$COLORSCRIPTSDIR/ghosts; read'
				guns	st -e sh -c '$COLORSCRIPTSDIR/guns; read'
				hex	st -e sh -c '$COLORSCRIPTSDIR/hex; read'
				hex-block	st -e sh -c '$COLORSCRIPTSDIR/hex-block; read'
				illumina	st -e sh -c '$COLORSCRIPTSDIR/illumina; read'
				jangofett	st -e sh -c '$COLORSCRIPTSDIR/jangofett; read'
				monster	st -e sh -c '$COLORSCRIPTSDIR/monster; read'
				mouseface	st -e sh -c '$COLORSCRIPTSDIR/mouseface; read'
				pacman	st -e sh -c '$COLORSCRIPTSDIR/pacman; read'
				panes	st -e sh -c '$COLORSCRIPTSDIR/panes; read'
				pinguco	st -e sh -c '$COLORSCRIPTSDIR/pinguco; read'
				pipes1	st -e sh -c '$COLORSCRIPTSDIR/pipes1; read'
				pipes2	st -e sh -c '$COLORSCRIPTSDIR/pipes2; read'
				pipes2-slim	st -e sh -c '$COLORSCRIPTSDIR/pipes2-slim; read'
				pukeskull	st -e sh -c '$COLORSCRIPTSDIR/pukeskull; read'
				rails	st -e sh -c '$COLORSCRIPTSDIR/rails; read'
				rally-x	st -e sh -c '$COLORSCRIPTSDIR/rally-x; read'
				rupees	st -e sh -c '$COLORSCRIPTSDIR/rupees; read'
				space-invaders	st -e sh -c '$COLORSCRIPTSDIR/space-invaders; read'
				spectrum	st -e sh -c '$COLORSCRIPTSDIR/spectrum; read'
				square	st -e sh -c '$COLORSCRIPTSDIR/square; read'
				tanks	st -e sh -c '$COLORSCRIPTSDIR/tanks; read'
				thebat	st -e sh -c '$COLORSCRIPTSDIR/thebat; read'
				thebat2	st -e sh -c '$COLORSCRIPTSDIR/thebat2; read'
				tiefighter1-no-invo	st -e sh -c '$COLORSCRIPTSDIR/tiefighter1-no-invo; read'
				tiefighter2	st -e sh -c '$COLORSCRIPTSDIR/tiefighter2; read'
				zwaves	st -e sh -c '$COLORSCRIPTSDIR/zwaves; read'
			cava	st -e cava
			pipes.sh	st -e pipes.sh
			rain.sh	st -e rain.sh
			unimatrix	st -e unimatrix -l Gg
			asciiquarium	st -e asciiquarium
			bonsai.sh	st -e bonsai -l -i -T
			eDEX-UI		sh -c (cd $HOME/Programs/edex-ui/ ; npm start)
	 Science
		 Astronomy
			Celestia	celestia
		 Biology
			Pymol		pymol
		 Chemistry
			ptable	st -e ptable
			chemtool	chemtool
		 Math
			Desmos	desmos
			Geogebra	geogebra
		Anki		anki
	 Development
		 IDEs
			Neovim	st -e nvim
			VS Code	code
			Dr. Racket	drracket
		 Github (TUI)	st -e github
		bitwise	bitwise
		Github Activity (TUI)	st -e sh -c 'ghcal --no-ansi ; read'
		QDbusViewer	qdbusviewer --style gtk2

 Configs
	 System
		 i3	st -e nvim $CONFIGDIR/i3/config
		 Start Menu	st -e nvim $HOME/dotfiles/dotfiles/scripts/xmenu.sh
		 Notifications	st -e nvim $CONFIGDIR/wal/templates/dunstrc
		 Smartphone	kdeconnect-settings --style gtk2
		 Sound	pavucontrol
		 Shell
			fish
				config.fish	st -e nvim $CONFIGDIR/fish/config.fish
				Web Config	fish -c fish_config
			bash	st -e nvim $HOME/.bashrc
		 polybar
			config	st -e nvim $CONFIGDIR/polybar/config
			launch.sh	st -e nvim $CONFIGDIR/polybar/scripts/launch.sh
		 rofi
			config	st -e nvim $CONFIGDIR/rofi/config
			template	st -e nvim $CONFIGDIR/wal/templates/custom-rofi.rasi
		 Utilities
			 khard	st -e nvim $CONFIGDIR/khard/khard.conf
			 khal	st -e nvim $CONFIGDIR/khal/config
			 vdirsyncer	st -e nvim $CONFIGDIR/vdirsyncer/config
		 .Xresources	st -e nvim $HOME/.Xresources
	 User
		ranger	st -e nvim $CONFIGDIR/ranger/rc.conf
		newsboat	st -e nvim $CONFIGDIR/newsboat/config
		neomutt
			neomuttrc	st -e nvim $CONFIGDIR/neomutt/neomuttrc
			colors	st -e nvim $CONFIGDIR/neomutt/colors
			settings	st -e nvim $CONFIGDIR/neomutt/settings
			mappings	st -e nvim $CONFIGDIR/neomutt/mappings
			mailcap	st -e nvim $CONFIGDIR/neomutt/mailcap
		neovim
			coc-settings	st -e nvim $HOME/.config/nvim/coc-settings.json
			functions	st -e nvim $HOME/.config/nvim/configs/functions.vim
			main	st -e nvim $HOME/.config/nvim/configs/main.vim
			mappings	st -e nvim $HOME/.config/nvim/configs/mappings.vim
			plugin-settings	st -e nvim $HOME/.config/nvim/configs/plugin-settings.vim
			plugins	st -e nvim $HOME/.config/nvim/configs/plugin.vim
		neofetch	st -e nvim $CONFIGDIR/neofetch/config.conf
		htop	st -e nvim $CONFIGDIR/htop/htoprc
		s-tui	st -e nvim $CONFIGDIR/s-tui/s-tui.conf
		spicetify	st -e nvim $CONFIGDIR/spicetify/config.ini
		stonks	st -e nvim $CONFIGDIR/stonks.yml

 System
	 Refresh i3		i3-msg restart
	 Refresh polybar		pkill -9 polybar ; $CONFIGDIR/polybar/scripts/launch.sh

	 Logout	i3-nagbar -t warning -m 'Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'
	 Shutdown		poweroff
	 Reboot			reboot
EOF

