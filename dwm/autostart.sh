#!/bin/bash
# Filename: autostart.sh
# Last Change: Mon, 19 Sep 2022 09:13:21

# If you are using this build it already has the autostart
# patch applyied, just create a folder ~/.dwm and put this
# file in there
#
#  [ ! -f ~/.dwm/autostart ] && (
#   mkdir ~/.dwm
#   ln -sfvn ~/.dotfiles/dwm/autostart.sh ~/.dwm/autostart.sh
#  )
#

picom -b --config ~/.config/picom/picom.conf &
/home/sergio/.local/bin/wal -qi ~/.dotfiles/backgrounds/
# numlockx on &
polkit-xfce-authentication-agent-1 &

## Disable touchpad while typing (voidlinux)
# doas cp /usr/share/X11/xorg.conf.d/70-synaptics.conf /etc/X11/xorg.conf.d/70-synaptics.conf
syndaemon -i .8 -K -t -R -d &

# 300 ms delay 50 chars per second
xset r rate 300 50 &

# disable screensaver
xset s off &

# siable power options
xset -dpms &

exec ~/.dotfiles/dwm/slstatus/slstatus
pgrep -u $UID -nf clipmenud || clipmenud &


