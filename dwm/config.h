/* See LICENSE file for copyright and license details. */
// Last Change: Sun, 18 Sep 2022 16:19:20

/* appearance */
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const unsigned int gappx     = 6;        /* gaps between windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "FontAwesome:size=11", "consolas:size=10" };
static const char dmenufont[]       = "monospace:size=10";
static const char col_gray1[]       = "#222222";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#bbbbbb";
static const char col_gray4[]       = "#eeeeee";
static const char col_cyan[]        = "#005577";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
	[SchemeSel]  = { col_gray4, col_cyan,  col_cyan  },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };
/*static const char *tags[] = {  "", "", "", "✅", "嗢", "", "", "", "" };*/

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class              instance    title       tags mask     switchtotag    isfloating   monitor */
	{ "Gimp",             NULL,       NULL,       0,            0,             1,           -1 },
	{ "Firefox",          NULL,       NULL,       2 << 0,       1,             0,           -1 },
	{ "TelegramDesktop",  NULL,       NULL,       1 << 4,       1,             0,           -1 },
	{ "ranger",           NULL,       NULL,       4 << 0,       1,             0,           -1 },
	{ "pulse",            NULL,       NULL,       0,            0,             1,           -1 },
	{ "St",               NULL,       NULL,       1 << 0,       1,             0,           -1 },
	{ "htop",             NULL,       NULL,       0,            0,             1,           -1 },
	{ "scratchpad",       NULL,       NULL,       0,            0,             1,           -1 },
	{ "scratchy",         NULL,       NULL,       0,            0,             1,           -1 },
	{ "nvim",             NULL,       NULL,       1 << 8,       1,             0,           -1 },
	{ "newsboat",         NULL,       NULL,       1 << 5,       1,             1,           -1 },
	{ "Nemo",             NULL,       NULL,       1 << 7,       1,             0,           -1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL };
static const char *termcmd[]  = { "st", "-f", "Iosevka Mayukai Original:size=15:antialias=true:autohint=true", NULL };
static const char scratchpadname[] = "scratchpad";
static const char *scratchpadcmd[] = { "st", "-c", scratchpadname, "-t", scratchpadname, "-g", "120x24", "-f", "Iosevka Mayukai Original:size=15:antialias=true:autohint=true", NULL };
static const char *rangercmd[]  = { "st", "-f", "Iosevka Mayukai Original:size=15:antialias=true:autohint=true", "-c ranger -e", "ranger" };
static const char *browcmd[]  = { "firefox", NULL };
static const char *nemocmd[]  = { "nemo", NULL };
static const char *htcmd[] = { "st", "-c htop -e", "htop" };
static const char *mixercmd[] = {"st", "-c pulse", "pulsemixer", NULL};
static const char *lockcmd[] = {"slock", NULL};
static const char *newsboatcmd[] = { "st", "-z 18 -c newsboat -e", "newsboat"};

/* Add somewhere in your constants definition section
 * source: https://bitbucket.org/sergio/workspace/snippets/KMndeG
*/
static const char *upvol[]   = { "/usr/bin/pactl", "set-sink-volume", "0", "+5%",     NULL };
static const char *downvol[] = { "/usr/bin/pactl", "set-sink-volume", "0", "-5%",     NULL };
static const char *mutevol[] = { "/usr/bin/pactl", "set-sink-mute",   "0", "toggle",  NULL };

#include <X11/XF86keysym.h>

static Key keys[] = {
	/* modifier                     key        function        argument */
    { 0,            XF86XK_AudioLowerVolume,   spawn,          {.v = downvol} },
    { 0,            XF86XK_AudioMute,          spawn,          {.v = mutevol }},
    { 0,            XF86XK_AudioRaiseVolume,   spawn,          {.v = upvol} },
    { MODKEY,                       XK_F11,    spawn,          {.v = downvol } },
    { MODKEY,                       XK_F12,    spawn,          {.v = upvol } },
    { MODKEY|ShiftMask,             XK_F12,    spawn,          {.v = mutevol } },
	{ ControlMask|Mod1Mask,         XK_Escape, spawn,          SHCMD("xkill")},
    { MODKEY,                       XK_v,      spawn,          SHCMD("st -c nvim -g 140x34 -e nvim")},
	{ ControlMask|Mod1Mask,         XK_r,      spawn,          SHCMD("rofi -show drun -show-icons")},
	{ ControlMask|Mod1Mask,         XK_b,      spawn,          SHCMD("/home/sergio/.local/bin/wal -qi ~/.dotfiles/backgrounds")},
	{ ControlMask|Mod1Mask,         XK_c,      spawn,          SHCMD("clipmenu")},
	{ ControlMask|Mod1Mask,         XK_f,      spawn,          SHCMD("/home/sergio/.dotfiles/dwm/scripts/rofi-files")},
    { MODKEY,                       XK_x,      spawn,          SHCMD("/home/sergio/.dotfiles/dwm/scripts/power-menu") },
    { MODKEY,                       XK_Print,  spawn,          SHCMD("/home/sergio/.dotfiles/dwm/scripts/screenshot_region.sh") },
    { ControlMask,                  XK_Print,  spawn,          SHCMD("/home/sergio/.dotfiles/dwm/scripts/screenshot_fullscreen.sh") },
    { MODKEY|ShiftMask,             XK_p,      spawn,          SHCMD("/home/sergio/.dotfiles/dwm/scripts/src-dmenu") },
	{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
	{ MODKEY|ControlMask,           XK_m,      spawn,          {.v = mixercmd } },
    { MODKEY,                       XK_s,      spawn,          {.v = lockcmd}},
	{ MODKEY,                       XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY|ShiftMask,             XK_t,      spawn,          {.v = htcmd } },
	{ MODKEY,                       XK_r,      spawn,          {.v = rangercmd } },
	{ MODKEY,                       XK_w,      spawn,          {.v = browcmd } },
	{ MODKEY,                       XK_n,      spawn,          {.v = nemocmd } },
	{ MODKEY|ShiftMask,             XK_n,      spawn,          {.v = newsboatcmd } },
	{ MODKEY,                       XK_u,      togglescratch,  {.v = scratchpadcmd } },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY|ShiftMask,             XK_j,      rotatestack,    {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_k,      rotatestack,    {.i = -1 } },
    { MODKEY|Mod1Mask,              XK_k,      spawn,          SHCMD("ps -o comm --no-headers x | uniq | rofi -dmenu | xargs killall")},
    { ControlMask|Mod1Mask,         XK_t,      spawn,          SHCMD("telegram-desktop")},
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,             XK_h,      setcfact,       {.f = +0.25} },
	{ MODKEY|ShiftMask,             XK_l,      setcfact,       {.f = -0.25} },
	{ MODKEY|ShiftMask,             XK_o,      setcfact,       {.f =  0.00} },
	{ MODKEY|ShiftMask,             XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ ControlMask|Mod1Mask,         XK_p,      view,           {0} },
	{ MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
    { ClkClientWin,   ControlMask|Mod1Mask,  Button1,   spawn,    SHCMD("xdotool click --delay 0 --window $(xdotool getactivewindow) --clearmodifiers 2") },
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

