#!/usr/bin/env bash
# File: /home/sergio/.dotfiles/dwm/scripts/screenshot_region.sh
# Last Change: Sun, 18 Sep 2022 16:15:55

# make sure we have scrot installed
[ ! $(command -v scrot) ] && doas xbps-install -Sy scrot

SD="$XDG_PICTURES_DIR/screenshots"

# make sure we have the screenshots dir
[ ! -d "$SD" ] && mkdir "$SD"

scrot -ozs -F "$SD/screenshot-region-%d-%b-%Y-%H-%M-%S-$wx$h.png" -e 'sleep 1;sxiv $f'

