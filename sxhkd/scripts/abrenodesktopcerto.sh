#!/usr/bin/env bash
#     Filename: abrenodesktopcerto.sh
#      Created: Mon, 04 Oct 2021 - 14:26
#  Last Change: Thu, 28 Apr 2022 09:32
#          vim: ft=sh:fdm=syntax:nu:

# Pay attention to the name because the bspc rule is case sensitive
# When you get an app name, like:

# xprop | awk '/WM_CLASS/{print $4}'
# it returns:
# "St"
# Which means, if you use a wrong name the rule will not work

DESKTOP=$(bspc query -D -d --names)
bspc rule -a \* -o desktop=^${DESKTOP} && $@
