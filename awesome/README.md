```txt
Filename: README.md
Last Change: Wed, 24 Aug 2022 09:12:34
```

## My new awesome tiling window manager config:
Read ~/.dotfiles/wiki/awesome.md

I have changed moved my plugins to "plugins" folder, and the imports now are:

    local lain = require("plugins.lain")
    local volume_control = require("plugins.volume-control")

# vim:ft=markdown:et:sw=4:ts=4:cole=0:

