```txt
# File: /home/sergio/.dotfiles/neofetch/README.md
# Last Change: Sun, 01 May 2022 07:46
tags: [tools, admin, sysadmin, info]
```

## Neofetch console:
+ https://linuxhint.com/what-is-neofetch-for-linux/ tags: [info, tools, admin, sysadmin]

Neofetch is a super-convenient command-line utility used to fetch system
information within a few seconds. It is cross-platform, open-source, and it
displays everything ranging from your system’s uptime to the Linux kernel
version. This guide will explain the working of Neofetch, its features, and
the method of installing it.

    doas xbps-install -Sy neofetch
