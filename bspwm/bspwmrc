#! /bin/sh
# last change: Fri, 16 Sep 2022 20:18:44
# ln -sfvn ~/.dotfiles/bspwn ~/.config
# sources:
# https://ricebr.github.io/Not-A-Blog//instalando-e-configurando-bspwm/
# https://blog.rzg.one/post/2021/05/09/floating-and-sticky-windows-in-bspwm/

# dependencies:
# pywal
# cava
# picom

bspc rule -r "*"

pgrep -x sxhkd > /dev/null || sxhkd &
pgrep -x gpm > /dev/null || doas gpm &
pgrep -u $UID -nf clipmenud$ || clipmenud &

# pgrep -x mpd > /dev/null || mpd &
# [ ! -s ~/.config/mpd/mpd.pid ] && mpd &
pgrep -x mpd > /dev/null || mpd &

# bspc monitor -d 1 2 3 4 5 6 #VII VIII IX X
# # bspc monitor -d 1 2 3 4 5
bspc monitor -d I II III IV V VI VII VIII IX X

bspc config border_width          2
bspc config window_gap            12

bspc config split_ratio           0.52
bspc config borderless_monocle    true
bspc config gapless_monocle       true
bspc config focus_follows_pointer true

# poniter modifier shift
# mover com shift botão esquerd
bspc config pointer action move
bspc config pointer_modifier shift
bspc config pointer_action1 move
bspc config pointer_action2 resize_side
bspc config pointer_action3 resize_corner

#Use Pywal on bspwm
# source the colors.
. "${HOME}/.cache/wal/colors.sh"

# Set the border colors.
bspc config normal_border_color "$color1"
bspc config active_border_color "$color2"
bspc config focused_border_color "$color15"
# Use the line below if you are on bspwm >= 0.9.4
bspc config presel_feedback_color "$color1"
# Use the line below if you are on bspwm < 0.9.4
#bspc config presel_border_color "$color1"

########## Border Colors #################
# bspc config focused_border_color "#131a1c"
# bspc config normal_border_color "#131a1c"

bspc rule -a St desktop='^1' follow=on focus=on
bspc rule -a Luakit desktop='^3' follow=on focus=on
bspc rule -a Firefox desktop='^2' follow=on focus=on state=tiled
bspc rule -a St:main desktop='^1' follow=on focus=on
# bspc rule -a St:curdesk state=floating
bspc rule -a firejail desktop='^2' follow=on focus=on
bspc rule -a Nemo desktop='^3' follow=on focus=on
bspc rule -a URxvt:ranger follow=on desktop=^3 state=tiled
bspc rule -a St:lf follow=on desktop=^3 state=tiled
bspc rule -a St:cmus follow=on desktop=^4 state=tiled
bspc rule -a St:ranger follow=on desktop=^3 state=tiled
bspc rule -a St:ranger follow=on desktop=^3 state=tiled
bspc rule -a Inkscape desktop='^4' follow=on focus=on state=tiled
bspc rule -a St:nvim desktop='^5' follow=on focus=on
bspc rule -a nvim desktop='^5' follow=on focus=on
bspc rule -a St:Neovim desktop='^5' follow=on focus=on
bspc rule -a St:htop state=floating
bspc rule -a *:Ranger follow=on desktop=^3 state=tiled
bspc rule -a Gimp desktop='^6' state=floating follow=on
bspc rule -a Sxiv desktop='^4' state=floating follow=on center=true rectangle=1200x700+0+0
bspc rule -a XClock:*:* state=floating
# bspc rule -a TelegramDesktop desktop='^7' state=tiled follow=on focus=on
bspc rule -a TelegramDesktop state=floating

# bspc rule -a St:cava state=floating center=true rectangle=500x300+0+0
bspc rule -a St:cava

bspc rule -a St:ncmpcpp state=floating center=true rectangle=1100x625+0+0
# bspc rule -a St:config state=floating center=true rectangle=1200x650+0+0
bspc rule -a St:config desktop=^6 state=tiled

# bspc rule -a St:scratchy sticky=on state=floating center=true rectangle=1100x625+0+0
bspc rule -a *:scratchy sticky=on state=floating center=true rectangle=1100x625+0+0
bspc rule -a St:scratchy sticky=on state=floating center=true rectangle=1100x625+0+0

## workspace 6 (multimedia)
bspc rule -a Audacity desktop='^6' follow=on focus=on
bspc rule -a Vlc desktop='^6' follow=on focus=on
# bspc rule -a mpv desktop='^6' follow=on focus=on state=floating center=true rectangle=widthxheight+Xoffset+Yoffset
bspc rule -a Ghb desktop='^6' follow=on focus=on
bspc rule -a Xfburn desktop='^6' follow=on focus=on
bspc rule -a *:progterm state=floating sticky=on

## workspace 7 (graphics-editors)
bspc rule -a Gimp desktop='^7' follow=on focus=on
bspc rule -a Gimp-2.10 desktop='^7' follow=on focus=on
bspc rule -a Inkscape desktop='^7' follow=on focus=on
bspc rule -a Oomox desktop='^7' follow=on focus=on

## need to be floating
bspc rule -a Audacious state=floating follow=on
bspc rule -a Viewnior state=floating follow=on
bspc rule -a feh state=floating follow=on
bspc rule -a Gpicview state=floating follow=on

## don't manage my conky & systemtray :/
bspc rule -a Conky state=floating manage=off
bspc rule -a stalonetray state=floating manage=off

xrdb ~/.Xresources &

# ${HOME}/.fehbg
# set a random wallpar after each restart
# feh -z --bg-fill ${XDG_PICTURES_DIR}/backgrounds/
# feh -z --bg-fill ~/img/backgrounds/ &
pkill feh > /dev/null
pkill wal > /dev/null
/home/sergio/.local/bin/wal -qi ~/.dotfiles/backgrounds/ &
# /bin/wal -qi ~/.dotfiles/backgrounds/ &

# NOTE: doas xbps-install -Sy libva-glx{,-devel} -- picom --backend glx &
picom -b --config ~/.config/picom/picom.conf
#picom -b
xset s off
xset -dpms

# # ~/.config/flashfocus/flashfocus.yml
# pkill flashfocus > /dev/null
# [ command -v flashfocus ] && $HOME/.local/bin/flashfocus &

# xcompmrg -c &
# xcompmgr -cfF -t-9 -l-11 -r9 -o.95 -D6 &
# killall -9 -qw picom 2>/dev/null; picom -b --backend glx &
# compton &
# ${HOME}/.config/polybar/launch.sh --forest &
# ${HOME}/.config/polybar/launch.sh --material &
#${HOME}/.config/polybar/launch.sh --hack &
# pkill polybar 2>/dev/null; $HOME/.config/polybar/launch.sh --hack &
# pkill polybar 2>/dev/null; polybar -q --config=/home/sergio/.config/polybar/config.ini &

# polybar with polybar-themes
# /home/sergio/.dotfiles/polybar-themes/simple/launch.sh --hack &
~/.config/polybar/launch.sh --hack &

pkill dunst
dunst ~/.config/dunst/dunstrc &

# # restart window titles daemon
# while pgrep -u $UID -f bspwm_window_titles >/dev/null; do pkill -f bspwm_window_titles; done
# bspwm_window_titles &

# uncoment the line location-provider=manual
pgrep -x redshift >/dev/null || redshift -l -35.3:149 &
# são josé de almeida - mg (right click on maps.google.com)
# pgrep -x redshift >/dev/null || redshift &
## get back brightness -> redshift -x

xsetroot -cursor_name left_ptr &

# restart window titles daemon
while pgrep -u $UID -f bspwm_window_titles >/dev/null; do pkill -f bspwm_window_titles; done
bspwm_window_titles &

setxkbmap -model abnt2 \
    -layout br -variant abnt2 \
    -option \
    -option "lv3:ralt_switch" \
    -option "terminate:ctrl_alt_bksp" \
    -option "caps:escape"

# pkill blurwal
# command -v blurwal && blurwal &
