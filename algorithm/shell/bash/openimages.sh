#!/bin/bash
# Last Change: Mon, 22 Aug 2022 09:51:00

# note: I don't know why this line only works as a script but does not work directly
# on my ~/.config/sxhkd/sxhkdrc

(
cd ~/img
sxiv ~/img/$(ls -1 | rofi -dmenu)
)
