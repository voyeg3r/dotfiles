-- Filename: matchparen.lua
-- Last Change: Tue, 14 Jun 2022 - 16:41

local ok, err = pcall(require, "matchparen")
if not ok then
    return
end

require('matchparen').setup({
    on_startup = true, -- Should it be enabled by default
    hl_group = 'MatchParen', -- highlight group for matched characters
    augroup_name = 'matchparen',  -- almost no reason to touch this unless there is already augroup with such name
})
