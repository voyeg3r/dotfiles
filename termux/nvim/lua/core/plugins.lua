-- /data/data/com.termux/files/home/.config/nvim/lua/core/plugins.lua
-- Last Change: Thu, 11 Aug 2022 17:40:24

local fn = vim.fn
local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
if fn.empty(fn.glob(install_path)) > 0 then
  packer_bootstrap = fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
  vim.cmd [[packadd packer.nvim]]
end

-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, 'packer')
if not status_ok then
    print("packer not loaded")
end


function get_setup(name)
    return string.format('require("plugins_setup/%s")', name)
end

-- Have packer use a popup window
packer.init {
    display = {
        open_fn = function()
            return require("packer.util").float { border = "rounded" }
        end,
    },
}

return require('packer').startup(function(use)

  -- packer can manag itself
  use 'wbthomason/packer.nvim'

  use {
      "lewis6991/impatient.nvim",
      rocks = "mpack",
  }

  -- dim inactive windows
  use 'sunjon/shade.nvim'

  -- Commenting
  use {
      'numToStr/Comment.nvim',
      config = function()
          require('Comment').setup()
      end,
  }

  use 'rcarriga/nvim-notify'
  use 'nathom/filetype.nvim'

  use "antoinemadec/FixCursorHold.nvim" -- This is needed to fix lsp doc highlight

  use({
      "karb94/neoscroll.nvim",
      opt = true,
      event = "WinScrolled",
      keys = {'<C-u>', '<C-d>',-- '<C-b>', '<C-f>',
      '<C-y>', '<C-e>', 'zt', 'zz', 'zb'},
      config = get_setup("neoscroll"),
  })

  use 'tjdevries/nlua.nvim'

  -- File explorer
  use ({'kyazdani42/nvim-tree.lua',
    config = get_setup("nvim-tree")
  })

  use({
      "kylechui/nvim-surround",
      config = function()
          require("nvim-surround").setup({
              -- Configuration here, or leave empty to use defaults
          })
      end
  })

  -- terminal
  use ({'numToStr/FTerm.nvim',
    config = get_setup("fterm")
  })

  use ({"akinsho/bufferline.nvim", config = get_setup("bufferline")})

  use ({"ldelossa/buffertag",
  config = function()
      require('buffertag').setup()
  end
  })

  use ({'monkoose/matchparen.nvim',
  -- opt = true,
  config = get_setup("matchparen")
  -- setup = function()
  --     require("core.utils").packer_lazy_load "matchparen.nvim"
  -- end,
  })

  -- Treesitter interface
  use ({'nvim-treesitter/nvim-treesitter',
    run = ':TSUpdate',
    config = get_setup("nvim-treesitter"),
  })

  use 'norcalli/nvim-colorizer.lua'

  use 'nvim-treesitter/nvim-treesitter-textobjects'

  use({ 'gbprod/substitute.nvim', -- exchange words
    config = get_setup("substitute")
  })

  -- Color scheme
  use ({'mcchrish/zenbones.nvim', requires = { "rktjmp/lush.nvim" } })

  -- Completion
  use "rafamadriz/friendly-snippets"
  use ({'onsails/lspkind-nvim', -- vscode-like pictograms
        config = get_setup("lspkind_rc")
      })
  use 'hrsh7th/cmp-buffer' -- nvim-cmp source for buffer words
  use 'hrsh7th/cmp-nvim-lsp' -- nvim-cmp source for neovim's built-in LSP
  use ({'hrsh7th/nvim-cmp', config = get_setup("cmp_rc") }) -- Completion
  use 'neovim/nvim-lspconfig' -- LSP
  use 'jose-elias-alvarez/null-ls.nvim' -- Use Neovim as a language server to inject LSP diagnostics, code actions, and more via Lua

  use ({'williamboman/mason.nvim',
    config = get_setup('mason_rc')
  })
  use 'williamboman/mason-lspconfig.nvim'

  use 'glepnir/lspsaga.nvim' -- LSP UIs
  use 'L3MON4D3/LuaSnip'

  use ({'MunifTanjim/prettier.nvim', -- Prettier plugin for Neovim's built-in LSP client
        config = get_setup("prettier_rc")
   })
  -- Autopair
  use {
      "windwp/nvim-autopairs",
      config = function() require("nvim-autopairs").setup {
        disable_filetype = { "TelescopePrompt" , "vim" },
      } end
  }

  use {
      'nvim-lualine/lualine.nvim',
      config = get_setup("lualine"),
  }

  -- Icons
  use ({'kyazdani42/nvim-web-devicons',
    config = get_setup('web-devicons_rc')
   })

  use ({'folke/zen-mode.nvim',
        config = get_setup("zenmode_rc")
   })

  use ({'lewis6991/gitsigns.nvim',
    config = get_setup("gitsigns_rc")
   })

  -- Telescope
  use ({"nvim-telescope/telescope.nvim",
  requires = {{'nvim-lua/plenary.nvim'}},
  config = get_setup("telescope"),
  -- module = "telescope",
  -- cmd = "Telescope",
  -- keys = {
  --     {"", "<C-p>"},
  --     {"", "<C-f>"},
  --     {"n", "<Leader>f"}
  -- },
  })

  -- Automatically set up your configuration after cloning packer.nvim
  -- Put this at the end after all plugins
  if packer_bootstrap then
      require('packer').sync()
  end
end)

