-- /data/data/com.termux/files/home/.config/nvim/lua/core/options.lua
-- Last Change: Thu, 11 Aug 2022 17:36:00

-- aliases
local opt  = vim.opt -- global
local g  = vim.g     -- global for let options

-- Using new filetype detection system(written in lua)
vim.g.do_filetype_lua = 1
vim.g.did_load_filetypes = 0
vim.g.python3_host_prog='/data/data/com.termux/files/usr/bin/python'


g.mapleader = ","

-- local colorscheme = "material"
local colorscheme = "duckbones"
local status_ok, _ = pcall(vim.cmd, "colorscheme " .. colorscheme)
if not status_ok then
  print("colorscheme " .. colorscheme .. " not avaiable")
end

vim.cmd([[inoreabbrev idate <C-R>=strftime("%b %d %Y %H:%M")<CR>]])
vim.cmd([[inoreab Fname <c-r>=expand("%:p")<cr>]])
vim.cmd([[inoreab Iname <c-r>=expand("%:p")<cr>]])
vim.cmd([[inoreab fname <c-r>=expand("%:t")<cr>]])
vim.cmd([[inoreab iname <c-r>=expand("%:t")<cr>]])

vim.cmd([[highlight MinhasNotas ctermbg=Yellow ctermfg=red guibg=Yellow guifg=red]])
vim.cmd([[match MinhasNotas /NOTE:/]])

-- disable builtins plugins
local disabled_built_ins = {
    "2html_plugin",
    "getscript",
    "getscriptPlugin",
    "gzip",
    "logipat",
    "matchit",
    "netrw",
    "netrwFileHandlers",
    "loaded_remote_plugins",
    "loaded_tutor_mode_plugin",
    "netrwPlugin",
    "netrwSettings",
    "rrhelper",
    -- "spellfile_plugin",
    "tar",
    "tarPlugin",
    "vimball",
    "vimballPlugin",
    "zip",
    "zipPlugin",
    "matchparen", -- matchparen.nvim disables the default
}

for _, plugin in pairs(disabled_built_ins) do
    vim.g["loaded_" .. plugin] = 1
end


local options = {
    ssop = vim.opt.ssop - { "blank", "help", "buffers" } + { "terminal" },
    modelines = 5,
    dictionary = vim.opt.dictionary + '~/.dotfiles/nvim/words.txt',   --  " C-x C-k C-n
    jumpoptions = opt.jumpoptions:append "stack",
    modelineexpr = false,
    keywordprg = ":help",
    autochdir = true,
    modeline = true,
    emoji = false, -- CREDIT: https://www.youtube.com/watch?v=F91VWOelFNE
    undofile = true,
    shada = "!,'30,<30,s30,h,:30,%0,/30",
    whichwrap = opt.whichwrap:append "<>[]",
    iskeyword = opt.iskeyword:append "-",
    shortmess = opt.shortmess:append { c = true },
    listchars = { eol = "↲", tab = "▶ ", trail = "•", precedes = "«", extends = "»", nbsp = "␣", space = "." },
    --completeopt = "menu,menuone,noselect",
    completeopt = { "menuone", "noselect"},
    encoding = "utf-8",    -- str:  String encoding to use
    fileencoding = "utf8", -- str:  File encoding to use
    syntax = "ON",        -- str:  Allow syntax highlighting
    foldenable = false,
    -- foldcolumn = "auto:9",
    foldopen = vim.opt.foldopen + "jump", -- when jumping to the line auto-open the folder
    foldmethod = "indent",
    --path = vim.opt.path + "~/.config/nvim/lua/user",
    --path = vim.opt.path + "**",
    -- https://issuecloser.com/blog/neovim-tip-smarter-path
    -- path = table.concat(vim.fn.systemlist("fd . -td"),","),
    path = vim.opt.path + "**",
    path = vim.opt.path + "../plugins_setup/**",
    wildignore = { ".git", ".hg", ".svn", "*.pyc", "*.o", "*.out", "*.jpg", "*.jpeg", "*.png", "*.gif", "*.zip" },
    wildignore = vim.opt.wildignore + { "**/node_modules/**", "**/bower_modules/**", "__pycache__", "*~", "*.DS_Store" },
    wildignore = vim.opt.wildignore + { "**/undo/**", "*[Cc]ache/" },
    wildignorecase = true,
    infercase = true,
    lazyredraw = true,
    showmatch = true,
    switchbuf = "useopen",
    matchtime = 2,
    synmaxcol = 128, -- avoid slow rendering for long lines
    pumheight = 10,
    pumblend = 15,
    wildmode = "longest:full,full",
    timeoutlen = 500,
    ttimeoutlen = 10, -- https://vi.stackexchange.com/a/4471/7339
    hlsearch = true, -- Highlight found searches
    ignorecase = true, -- Ignore case
    inccommand = "nosplit", -- Get a preview of replacements
    incsearch = true, -- Shows the match while typing
    joinspaces = false, -- No double spaces with join
    linebreak = true, -- Stop words being broken on wrap
    list = false, -- Show some invisible characters
    relativenumber = true,
    scrolloff = 2, -- Lines of context
    shiftround = true, -- Round indent
    shiftwidth = 4, -- Size of an indent
    expandtab = true,
    showmode = false, -- Don't display mode
    sidescrolloff = 8, -- Columns of context
    -- signcolumn = "yes:1", -- always show signcolumns
    smartcase = true, -- Do not ignore case with capitals
    smartindent = true, -- Insert indents automatically
    spell = false, -- enable spell suggestions
    spelllang = { "en_us" },
    spellsuggest = vim.opt.spellsuggest:append "8",
    splitbelow = true, -- Put new windows below current
    splitright = true, -- Put new windows right of current
    tabstop = 4, -- Number of spaces tabs count for
    termguicolors = true, -- You will have bad experience for diagnostic messages when it's default 4000.
    wrap = true,
    mouse = "a",
    undodir = vim.fn.stdpath("cache") .. "/undo",
    updatetime = 300, -- faster completion
    fillchars = {
    vert = "▕", -- alternatives │
    fold = " ",
    eob = "~", --  ~ at EndOfBuffer
    diff = "╱", -- alternatives = ⣿ ░ ─
    msgsep = "‾",
    foldopen = "▾",
    foldsep = "│",
    foldclose = "▸",
    },
    laststatus = 3,
    -- autoread = true,    -- detects file change by other apps
}

for k, v in pairs(options) do
    vim.opt[k] = v
end

-- window-local options
local window_options = {
    numberwidth = 2,
    number = true,
    relativenumber = true,
    linebreak = true,
    -- cursorline = true,
    foldenable = false,
    -- winbar = "%{%v:lua.require'core.winbar'.statusline()%}",
    -- winbar = "%=" .. "%m" .. " " .. "%F" .. " ",
}

for k, v in pairs(window_options) do
    vim.wo[k] = v
end

