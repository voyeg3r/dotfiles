-- Filename: lua.lua
-- Last Change: Thu, 11 Aug 2022 17:39:16
-- reference: r/neovim/comments/pl0p5v/comment/hvn0kff/

-- local opt_local = vim.opt_local
-- vim.opt_local.includeexpr , _ = vim.v.fname:gsub('%.', '/')
vim.cmd [[ setlocal includeexpr=substitute(v:fname,'\\.','/','g') ]]
vim.opt_local.suffixesadd:prepend '.lua'
vim.opt_local.suffixesadd:prepend 'init.lua'
vim.opt_local.spell = true
vim.bo.path = vim.o.path .. ',' .. vim.fn.stdpath('config')..'/lua'

