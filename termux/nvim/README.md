#!/bin/zsh

# alias trash='gio trash'

#trash ~/.config/nvim

trash ~/.config/nvim/plugin/packer_compiled.lua
trash ~/.cache/nvim
trash ~/.local/share/nvim

git clone --depth 1 https://github.com/wbthomason/packer.nvim\
 ~/.local/share/nvim/site/pack/packer/start/packer.nvim

nvim --headless -c 'packadd packer.nvim' -c 'quitall'
nvim --headless -c 'autocmd User PackerComplete quitall' -c 'PackerSync'
nvim --headless -c 'UpdateRemotePlugins' -c 'quitall'
