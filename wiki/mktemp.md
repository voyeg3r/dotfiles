``` txt
file: mktemp.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

O comando mktemp permite criar arquivos temporários seguros

cd $(mktemp -d)

mktemp /tmp/example.XXXXXXXXXX
## Cada X será substituido por um numero randômico único

TMPFILE=`mktemp  /tmp/datewatch.XXXXXXXXXX`

