```txt
File: /home/sergio/.dotfiles/wiki/firejail.md
Last Change:  Fri, 06 May 2022 - 19:15:54
tags: [security, hack, tools]
 ```

## what is firejail?
+ https://www.youtube.com/watch?v=MVLrclfbS4U

Firejail is an easy to use SUID sandbox program that reduces the risk of security breaches by restricting the running environment of untrusted applications using Linux namespaces, seccomp-bpf and Linux capabilities.

## Using Firejail by default

To use Firejail by default for all applications for which it has profiles, run the firecfg tool with sudo:

    $ sudo firecfg
