``` txt
file: gpsd.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39

# gpsd
+ http://www.linux-magazine.com/Issues/2018/210/Tutorial-gpsd

    Interface and daemon for GPS receivers

## for a voidlinux distro enables gpsd service

    sudo ln -s /etc/sv/gpsd /var/service

