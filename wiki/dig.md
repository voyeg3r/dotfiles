``` txt
file: dig.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

# dig - fornece informações de DNS

## External ip (wanip)

    alias wanip='dig @resolver1.opendns.com ANY myip.opendns.com +short'
