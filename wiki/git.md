```txt
file: git.md
author: Sergio Araujo
Last Change: Tue, 16 Aug 2022 11:52:29
tags: [git, github, dev]
```

## git intro:
+ https://www.alexkras.com/19-git-tips-for-everyday-use/
+ [primeiros passos](https://git-scm.com/book/pt-br/v1/Primeiros-passos-Uma-Breve-Hist%C3%B3ria-do-Git)
+ https://youtu.be/USjZcfj8yxE (git step by step tutorial)

## list all tracked files

    git ls-files

    git ls-tree --full-tree --name-only -r HEAD | tree --fromfile .

## How do I fix a Git detached head?

    git commit -m "....."
    git branch my-temporary-work
    git checkout master
    git merge my-temporary-work
    git branch -d my-temporary-work

## git bare repository
+ https://developer.atlassian.com/blog/2016/02/best-way-to-store-dotfiles-git-bare-repo/

```bash
[ -d $HOME/.cfg ] || mkdir $HOME/.cfg
git init --bare $HOME/.cfg
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
config config --local status.showUntrackedFiles no
echo "alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'" >> $HOME/.zshrc
```

## how to make git store my credentials?
+ https://git-scm.com/book/en/v2/Git-Tools-Credential-Storage

## retornar repositório para uma data específica
+ https://stackoverflow.com/a/6990682/2571881

  git checkout `git rev-list -n 1 --first-parent --before="2019-07-27 13:37" master`

```bash
git checkout $(
  git log --reverse --author-date-order --pretty=format:'%ai %H' master |
  awk '{hash = $4} $1 >= "2019-11-01" {print hash; exit 0 }'
)
```

## Revert to an espefific commit:
+ https://devopscube.com/checkout-clone-specific-git-commit-id-sha/

1 - Get the commit ID (SHA) that you want to checkout.

    git log

2 - Let's say: 28cd74decb47d8a5294eb59e793cb24a0f242e9e

    git checkout 28cd74decb47d8a5294eb59e793cb24a0f242e9e

You can also use the short form of the SHA ID from the start, as shown below.

    git checkout 28cd74de

## add new files:

    git status --short | awk '$1 ~ /^\?\?/  {print "git add ",$2}' | sh

## Show git branches

    git branche -a

## Create a new branch from the master branch

To create a new branch from a master branch, just use the following Git command:

    git branch new-branch-name

## Checkout the new branch (or creating a new branch)

Checkout your new Git branch by using the checkout option for the command:

    git checkout new-branch-name

Or

    git checkout -b new-branch-name

    git push --set-upstream origin new-branch-name

## Rename a Git branch with the -m command option

The Git rename command will require you to ad the -m option to your command:

    git branch -m new-name

## You are in detached HEAD state and how to fix in git
+ https://learnwebtutorials.com/you-are-in-detached-head-state-how-fix

To recover, create a branch named “temp” and switch to it…

    git branch temp
    git checkout temp

By switching to temp, you are attaching HEAD to the temp branch.

Git status now will show “On branch temp”. This is good.

1.  You want to be back on master, so switch to master …

git checkout master

2.  And then delete the temp branch (see tutorial)…

git branch -d temp

3.  Now you are back to HEAD on master. If you still want to adandon current file change, pull from repository with the
    command …

    git checkout — <filename>

### Rename a Git branch from another branch

You can also rename a branch from another branch. The following example shows, how to rename a Git branch from the
master branch:

    git checkout master

    git branch -m old-name new-name

## Remove Local Git Branch

To remove a local branch, you can use either of the following Git commands.

    git branch -d branch_name

## Update a single file from another git branch

    # On branch master
    git checkout gh-pages
    git checkout master -- myplugin.js
    git commit -m "Update myplugin.js from master"

## Clonar com submódulos
+ https://stackoverflow.com/a/4438292

  git clone --recursive -j8 https://github.com/voyeg3r/dotfaster.git ~/.dotfiles git fetch --recurse-submodules --jobs=8
  git submodule foreach git pull

  git submodule init git submodule update

  ssh -T git@github.com var=`echo 3gmail.com@ | sed 's,\(^3\)\(gmail\.com\)\(\@\),voyeg\1r\3\2,g'` git config --global
  user.name voyeg3r git config --global merge.tool vimdiff git config --global user.email ${var} git config --global
  alias.last 'log -1 HEAD' git config --global credential.helper cache git config --global push.default simple git
  config --global credential.helper 'cache --timeout=3600' git config --global alias.hist 'log --pretty=format:"%h %ad |
  %s%d [%an]" --graph --date=short'

## Atualizar submodules - update submodules:

    git submodule foreach git pull

## force a git pull
+ https://stackoverflow.com/a/8888015/2571881

If you have any files that are not tracked by Git (e.g. uploaded user content), these files will not be affected.

I think this is the right way:

    git fetch --all

Then, you have two options:

    in my case (main)
    git reset --hard origin/master

### Force a git pull for a single file

    git fetch
    git checkout origin/master <filepath>

## Git show files that were changed in the last 2 days
- https://stackoverflow.com/questions/7499938/

  git log --pretty=format: --name-only --since="2 days ago" | sort | uniq

## Using vim as a pager for git
+ https://www.reddit.com/r/vim/comments/ri7lj/using_vim_as_a_pager_for_git/

  git log --follow -p -- bin/aleatorios.py | vim - -R -c 'set foldmethod=syntax'

  git log -p $@ | vim - -R -c 'set foldmethod=syntax'

The `'$@'` inserts the full string of arguments you passed to the command. If you save this as 'git-log' and make it
executable, you can call it like this:

    $ git-log
    $ git-log -40
    $ git-log --since=1week

## update local repo with github updates

    git pull origin master

## Shallow clone

    git clone --depth 1 https://github.com/voyeg3r/dotfiles

## git log

    # shows you the last commit message "only"
    git log -1 --pretty=%B

    # Shows each commit message and its hash code
    git log --oneline --graph

You can get the hash for the last 5 commits this way:

     git log --oneline -5

     8a57072 (HEAD -> master, origin/master, origin/HEAD) updated gitmodules
     169cd33 Auto commit of init.vim.
     82078e0 eliminado terminalsettings.vim
     6983330 Auto commit of init.vim.
     f9856b2 Auto commit of init.vim.

## See last commit in context

    git show

## see diferences before commit

    git diff file

## short hash version of the last commit
+ https://stackoverflow.com/a/5694416/2571881

```markdown
    git rev-parse --short HEAD
```

This way we can get some information about the last commit

    git diff-tree -p $(git rev-parse --short HEAD) | less

## unstag file before commit:
Para arquivos que você adicionou à árvore e depois desistiu de eviar

    git restore --staged zsh/plugins/zsh-extract

## como ver o que mudou em um arquivo

    git log --follow -p --  bin/get-1000-phrases.sh

## see last commit message

    git log -1
    git log --oneline -1

## Revert a commit, softly
- https://bit.ly/2HqXn9P

  git revert -n

### Git – Revert to Specific Commit

Find the specific commit you want to revert all changes to:

    $ git log --oneline

Rollback all changes to that old commit:

    $ git checkout be9055b .

Note: The dot (.) after the branch name is mandatory.

Add the changes to the staging area and commit them:

    $ git add -A
    $ git commit -m "Revert commit: be9055b"

Once the local rollback is done, you can revert the already pushed commits, by pushing a new revision with the reverted
changes to the remote Git repository:

    $ git push

## Amend last commit message: (corrigir texto do último commit)

To change the last commit, you can simply commit again, using the --amend flag:

    git commit --amend -m "New and correct message"

## acessar uma linha específica de um arquivo do github

    https://github.com/atomaka/dotfiles/blob/master/Makefile#L10

    Como visto acima basta colocar no final da url #Ln  "onde n é
    o número da linha"

## Submódulos
+ https://stackoverflow.com/a/21195182/2571881

  git submodule add --force https://github.com/zsh-users/zsh-autosuggestions.git

  # after some changes at master

  git pull

Or, if you're a busy person:

    git submodule update --remote --merge

    git submodule add https://github.com/zsh-users/zsh-completions.git zsh/plugins/zsh-completions

## Clonando um repo e também os submódulos em paralelo
+ https://stackoverflow.com/a/34762036/2571881

  git clone --recursive repo git fetch --recurse-submodules --jobs=4

## Download a single folder or directory from a GitHub repo
+ https://stackoverflow.com/questions/7106012

You can use svn for that purpose

Let's say you want download FiraMono from nerd-fonts repo, you have just to replace `tree/master` with `trunk`

    https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts/FiraMono

Protip: You can use svn ls to see available tags and branches before downloading if you wish

    svn ls https://github.com/ryanoasis/nerd-fonts/trunk/patched-fonts/FiraMono
    svn checkout https://github.com/ryanoasis/nerd-fonts/trunk/patched-fonts/FiraMono

## How to use git with gnome-keyring integration

    sudo apt-get install libgnome-keyring-dev
    cd /usr/share/doc/git/contrib/credential/gnome-keyring
    sudo make
    git config --global credential.helper /usr/share/doc/git/contrib/credential/gnome-keyring/git-credential-gnome-keyring

Just add these two lines to your ~/.gitconfig file:

    [credential]
        helper = gnome-keyring

## How to save username and password in git
+ https://stackoverflow.com/a/35942890/2571881

  git config credential.helper store

then

    git pull

provide user-name and password and those details will be remembered later. The credentials are stored in the disk, with
the disk permissions.

if you want to change password later

    git config credential.helper store

then

     git pull

provide new password and it will work like before.

## List/show git configuration

    git config --list

## Git clone particular version of remote repository
+ https://stackoverflow.com/a/3555202/2571881

  git clone [remote_address_here] my_repo cd my_repo git reset --hard [ENTER HERE THE COMMIT HASH YOU WANT]

## Desfazendo coisas

## Desfazer alterações em um arquivo

    git checkout -- file
