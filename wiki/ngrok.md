```txt
Filename: ngrok.md
Last Change: Mon, 15 Aug 2022 22:45:58
tags: [network, ssh]
```

## Get ngrok token:

https://dashboard.ngrok.com/get-started/your-authtoken

    ngrok tcp 22

    It will show something like:

    Forwarding      tcp://0.tcp.sa.ngrok.io:19816 -> localhost:22

    ssh -p 19816 user@0.tcp.sa.ngrok.io

