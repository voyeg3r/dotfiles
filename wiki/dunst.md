---
File: dunst.md
Author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
tags: [tools, notification]
---

Lightweight and customizable notification daemon

    ln -sfvn /home/sergio/.dotfiles/dunst /home/sergio/.config
