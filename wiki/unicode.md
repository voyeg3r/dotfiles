```txt
-- File: /home/sergio/.dotfiles/wiki/unicode.md
-- Last Change:  Tue, 26 Apr 2022 - 13:07:39
tags: [utf-8, unicode, vim, nvim]
```

	╭─────────────────────────────────────╮
	│                                     │
	│  One text box drowed using neovim   │
	│                                     │
	╰─────────────────────────────────────╯

 u256d --> ╭

 u2570 --> ╰

 u256e --> ╮

 u2500 --> ╯

 u2500 --> ─
