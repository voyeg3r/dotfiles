File: /home/sergio/.dotfiles/wiki/xev.md
Last Change: Sep 17 2022 19:04
tags: [cli, tools, keyboard]

xev # This X program is useful for seeing the keycodes and symbols generated by your keyboard. Can remap using xmodmap. For instance, I've changed my caps lock key into an escape key.
