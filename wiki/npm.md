``` txt
file: npm.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

# npm.md
npm is the default package manager for Node.js.

## npm definition in Portuguese

O npm é o Gerenciador de Pacotes do Node (Node Package Manager) que vem junto
com ele e que é muito útil no desenvolvimento Node. Por anos, o Node tem sido
amplamente usado por desenvolvedores JavaScript para compartilhar ferramentas,
instalar vários módulos e gerenciar suas dependências. Sabendo disso, é
realmente importante para pessoas que trabalham com Node.js entendam o que é
npm.

## latex like websites
+ https://latex.now.sh/#getting-started

    npm install latex.css

