``` txt
file: join.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

# join.md intro

join lines of two files on a common field

				join -v 2 <(sort A.txt) <(sort B.txt)
