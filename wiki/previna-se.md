``` txt
file: previna-se.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```


Francisco Cândido Xavier - Agenda Cristã
Pelo Espírito André Luiz

    PREVINA-SE

Equilibre sua justiça, subtrain do-lhe as inclinações para a vingança.
Acautele-se com o seu desassombro, para não cair em temeri-dade.
Analise sua firmeza, para que se não transforme em petrifica-ção.
Ilumine suas diretrizes, a fimde que se não convertam em despotismo.
Examine sua habilidade, evitando-lhe a internação em velhacaria.
Estude sua dor para que não seja revolta.
Controle seus melindres, de modo que se não instalem na ca-sa sinistra do ódio.
Vele por sua franqueza, a fim de que a sua palavra não destile veneno.
Vigie seu entusiasmo para que não constitua imponderação.
Cultive seu zelo nobre, mas não faça dele uma cartilha escura de violência

tags: espiritismo , orações, preces
