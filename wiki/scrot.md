---
File: scrot.md
Author: Sergio Araujo
Last Change:  Tue, 17 May 2022 - 18:52:59
tags: [tools, img, screenshot, graphics]
---

## command line screenshot tool
+ https://www.reddit.com/r/bspwm/comments/pbcuwy/screenshots_in_bspwm/
+ https://wiki.archlinux.org/title/Screen_capture#Screenshot_software
+ https://www.howtoforge.com/tutorial/how-to-take-screenshots-in-linux-with-scrot/

    scrot $XDG_PICTURES_DIR/screenshots/Screenshot_%Y-%m-%d-%S_$wx$h.png -e 'sleep 1;sxiv $f'

