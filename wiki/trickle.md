``` txt
file: trickle.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

# Limitando o apt update a 10k

    trickle -s -d11 apt-get update

## Limitando downloads no firefox a 15k

    trickle -s -d15 firefox

## Trickle with git
That is useful for download large projects

    trickle -sd 50 git clone
    trickle -sd 50 git clone https://github.com/ryanoasis/nerd-fonts.git

## Referências
* http://linuxhard.org/site/archives/1044

tags: tools, network
