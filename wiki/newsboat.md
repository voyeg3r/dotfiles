File: /home/sergio/.dotfiles/wiki/newsboat.md
Last Change: Sat, 17 Sep 2022 13:44:40

Command Line RSS reader


## Meus atalhos do newsboat:

    n ................ next unread
    K ................ prev
    o ................ open in browser
    ,v ............... open video on mpv
    r ................ reload current feed
    R ................ reload all feeds
