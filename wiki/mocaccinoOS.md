``` txt
File: mocaccinoOS.md
Author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
tags: [linux, distros]
```

#  mocaccinoOS.md intro:
It is a Gentoo distro linux based on Sabayon Linux

## Package Management
+ https://www.mocaccino.org/docs/desktop/usage/

    luet install <package_name>

    luet uninstall <package_name>

    luet upgrade

    luet search <regex>

To look into the installed packages:

    luet search --installed <regex>


