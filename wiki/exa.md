``` txt
File: exa.md
Author: Sergio Araujo
Last Change:  Wed, 11 May 2022 - 09:38:19
Date: nov 21, 2020 - 20:35
tags: [linux, files, tools]
```

##  exa.md -> Modern replacement for ls
+ https://the.exa.website/

Exa – A Modern Replacement for “ls Command” Written in Rust

Exa is a lightweight, fast and modern replacement for the popular ls command
on Unix-like operating systems. It is written in Rust programming language
and comes with several additional features not available in the traditional
ls command.

    exa -l
    exa -bghHliS

    exa -T  ............. shows a tree
    exa --icons ......... shows icons
    exa --long --color=never
