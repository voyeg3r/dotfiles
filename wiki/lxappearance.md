```txt
Filename: lxappearance.md
Last Change: Mon, 22 Aug 2022 22:07:09
tags: [desktop, gtk, theme]
```

lxappearance - GTK+ theme switcher

LXAppearance is part of LXDE project.
It's a desktop-independent theme switcher for GTK+.
