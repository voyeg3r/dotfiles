``` txt
file: iwlist.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

# procurar redes wireless disponíveis

    iwlist device scanning
    iwlist wlan0 scanning

como pegar informações das redes sem fio
iwlist wlan0 scan

    iwconfig wlan0 essid 'nomedaredesemfio'

## Referencias
+ va.mu/bRcZ
