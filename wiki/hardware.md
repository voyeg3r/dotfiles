``` txt
File: hardware.md
Author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
Date: ago 02, 2021 - 21:28
tags: [hardware]
```

#  hardware.md intro:
Site to find out the socket of your processor, as long as you know it you can choose another processor with the same socket.

https://www.cpu-world.com/
