``` txt
file: pgrep.md
author: Sergio Araujo
Last Change:  Tue, 24 May 2022 - 10:31:16
```

##  pgrep.md intro:

    pgrep -x mysqld >/dev/null && echo "Process found" || echo "Process not found"

    The `-x` flag makes `pgrep` show just the process ID "pid"

    pgrep -u $UID -f clipmenud

    -f, --full
        The pattern is normally only matched against the
        process name.  When -f is set, the full command line
        is used.

