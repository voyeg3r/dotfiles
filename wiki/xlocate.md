## xlocate (from xtools package)

After installing xtools run:

    xlocate -S

Then you can find what package supports "pidof" for example:

pidof is in the procps-ng package. To search for a package which includes a given binary, you can use the xlocate tool from xtools.

    ~ % xlocate /usr/bin/pkill
    procps-ng-3.3.17_2	/usr/bin/pkill


pidof is in the procps-ng package. To search for a package which includes a given binary, you can use the xlocate tool from xtools.

~ % xlocate /usr/bin/pkill
procps-ng-3.3.17_2	/usr/bin/pkill
