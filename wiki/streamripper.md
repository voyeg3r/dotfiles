```txt
-- File: /home/sergio/.dotfiles/wiki/streamripper.md
-- Last Change: Thu, 28 Jul 2022 19:34:27
 vim: cole=0:
tags: [audio, mp3, stream, radio]
```

## Rip Streaming Audio
+ https://www.oreilly.com/library/view/linux-multimedia-hacks/0596100760/ch04s10.html

Streamripper lets you rip the live streaming audio station of your choice directly to MP3 for later listening.

Streaming audio has allowed a number of people to easily broadcast not only their favorite music tracks, but also other types of radio shows. Instead of being broadcast all day long, some shows are broadcast only at certain times of the day. If you aren’t at your computer when the show is broadcast you’ll miss out—unless you have streamripper.

streamripper is a simple but powerful command-line application that allows you to record streaming audio directly to local MP3 files. To install streamripper, see if your distribution has already packaged it, otherwise download the source from the official page at http://streamripper.sourceforge.net and compile and install it according to the installation instructions.

To use streamripper, simply pass it the URL to your streaming audio station in a console window:

	$ streamripper http://69.56.219.92:8072

By default, streamripper will create a directory named after the stream in the current directory and then start storing the streaming content as MP3s within an incoming directory. When a file is complete, streamripper will move it up from the incoming directory to the stream’s main directory. Each file is named after the artist and track metadata that streamripper grabs from the audio stream. You can leave streamripper running as long as you wish (provided you have enough hard drive space), and it will continue to grab and store MP3s in the stream’s directory. Provided you have enough band-width, you can even launch multiple instances of streamripper and simultaneously capture multiple streams.

The streamripper defaults are suitable for standard uses, but streamripper also allows you to configure everything through command-line options. For instance, the -d argument tells streamripper to rip files into the specified directory instead of the current working directory. The -s argument tells streamripper not to create a directory for each stream, and instead to save all of the MP3s into a single directory.

By default, MP3s that streamripper saves are titled after the artist and track name only. For some audio streams you might want to store the files in the order they were played on the stream. The -q option will cause streamripper to add a sequence number to the beginning of each filename starting from 001. In addition the -P prefix argument will let you add a particular string to the beginning of each file. Use the -a filename argument, and streamripper will store the entire stream to a single large file in addition to multiple files. Add the -A argument, and streamripper will only store to the single file and won’t create the individual files.

## see also: https://www.nongnu.org/streamtuner/

## How to use streamripper:

```txt
finding a stream
----------------

"The easiest way to get started is to find the URL of a stream you want to rip.
Usually I find the URL by loading it up in Winamp or XMMS and querying for the source URL."

commands
--------

# specify directory
streamripper URL -d recordings

# don't create a directory for each stream
-s

# add sequence number to output file
-q

# suppress the creation of the stream folder by adding -s
streamripper URL -d OutputFolder -s

# run for an hour
-l 3600

# If -D is used, the options -s and -P will be ignored.
# name files with date and time (per exec)
-D %d

%S   Stream
%A   Artist
%T   Title
%a   Album

-a [pattern]
# Sometimes you want the stream recorded to a single (big) file
  without splitting into tracks. The -a option does this. If you use -a without
  including the [pattern], a timestamped filename will automatically be used.

-A
Don´t create individual tracks.
The default mode of operation is to create one file for each track.
But sometimes you don´t want these files.
Using the -A option, the individual files for each track are not created.

# user agent
-u "FreeAmp/2.x"

-o (always | never | larger | version)
Overwrite tracks in `complete` directory. When streamripper rips tracks they are put into
the incomplete directory until they are finished. Normally, they are then moved into the
complete directory. However, when the track is already there, can use this option to tell
streamripper what you want to do.

--quiet
Quiet operation. Don´t write any text to the console, except error messages

--stderr
Write output to stderr instead of stdout

tests
-----

streamripper http://4533.live.streamtheworld.com:80/KKFNFMAAC_SC -d recordings -l 5 -s -q -D %S%d

# creates "recordings/foobar/incomplete/ - .aac"
streamripper http://4533.live.streamtheworld.com:80/KKFNFMAAC_SC -d recordings -l 5 -s -q -D foobar

streamripper http://4533.live.streamtheworld.com:80/KKFNFMAAC_SC -d recordings -l 5 -D 104_3

# creates "recordings/104_3.aac"
streamripper http://4533.live.streamtheworld.com:80/KKFNFMAAC_SC -d recordings -l 3 -D 104_3 -a 104_3

date=`date +"%Y_%m_%d"`
streamripper http://4533.live.streamtheworld.com:80/KKFNFMAAC_SC -d recordings -l 3 -D 104_3.${date} -a 104_3.${date}
```


