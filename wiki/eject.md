``` txt
file: eject.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

# Remontar pendrive sem retira-lo

eject /dev/sdb; sleep 1; eject -t /dev/sdb

## abrir bandeja de cd

eject

## fechar bandeja de cd

eject -t

