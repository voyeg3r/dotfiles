```txt
File: /home/sergio/.dotfiles/wiki/dwm.md
Last Change: Sun, 11 Sep 2022 13:36:12
tags: [desktop managers]
```

## dwm links:
+ https://www.youtube.com/watch?v=7pfxHoAnCkE (distrotub video)
+ https://blog.juancastro.xyz/en/posts/dwm/ (great article)
+ https://github.com/skuzzymiglet/dwm/blob/master/config.h
+ https://github.com/tallguyjenks/dwm/blob/main/config.h

Para que o script de autostart carregue crie uma pasta
~/.dwm contendo o script de autostart

Currently I am using the distrotube build with some changes.
I am also testing the flexypatch build:

https://github.com/bakkeby/dwm-flexipatch

## patches:

1 - SwichtTag: https://dwm.suckless.org/patches/switchtotag/

    Adds a rule option to switch to the configured tag when a window opens, then
    switch back when it closes.

    Example Configuration

    static const Rule rules[] = {
        /* class      instance title tags mask switchtotag isfloating monitor */
        { "Gimp",     NULL,    NULL, 0,        0,          1,         -1 },
        { "Firefox",  NULL,    NULL, 1 << 8,   1,          0,         -1 },
    };

2 - alwayscenter - https://dwm.suckless.org/patches/alwayscenter/
3 - attachbottom
4 - autostart
5 - movestack
6 - pertag (different layouts for each tag)
7 - scratchpad - https://dwm.suckless.org/patches/scratchpad/
8 - uselessgap https://dwm.suckless.org/patches/uselessgap/dwm-uselessgap-20211119-58414bee958f2.diff

## Swich to the last active tag

    super + alt + j
	{1, {{MODKEY|Mod1Mask, XK_j}},		  view,           {0} },


## Add dwm in your login manager:
+ https://www.reddit.com/r/suckless/comments/jj61py/

    sudo <<-EOF > /usr/share/xsessions/dwm.desktop
    [Desktop Entry]
    Encoding=UTF-8
    Name=dwm
    Comment=Dynamic window manager
    Exec=dwm
    Icon=dwm
    Type=XSession
    EOF

    sudo chmod 644 /usr/share/xsessions/dwm.desktop

## dwmblocks - dwm status bar
+ https://www.youtube.com/watch?v=h2op4QqIuHk

## Paste primary selection on mouse click:

    { ClkClientWin,   ControlMask|Mod1Mask,  Button1,   spawn,    SHCMD("xdotool click --window $(xdotool getactivewindow) --clearmodifiers 2") },

## how to configure dwm:
+ https://dev.to/knassar702/the-best-way-to-configure-your-dwm-5efn
+ https://ratfactor.com/dwm2

## Luck Smith dwm:

    Mod + F1  ................ display help
    Super + F ................ toggle full screen
    q ........................ to quit

    Notice also the case sensitivity of the shortcuts* , Be sure you play around with these. Be
flexible with the basic commands and the system will grow on you quick.

    • Mod+Enter – Spawn terminal (the default terminal is st; run man st for more.)
    • Mod+q – Close window
    • Mod+d – dmenu (For running commands or programs without shortcuts)
    • Mod+j/k – Cycle thru windows by their stack order
    • Mod+Space – Make selected window the master (or switch master with 2nd)
    • Mod+h/l – Change width of master window
    • Mod+z/x – Increase/decrease gaps (may also hold Mod and scroll mouse)
    • Mod+a – Toggle gaps
    • Mod+A – Gaps return to default values (may also hold Mod and middle click)
    • Mod+Shift+Space – Make a window float (move and resize with Mod+left/right click).

• Mod+s – Make/unmake a window “sticky” (follows you from tag to tag)
• Mod+b – Toggle statusbar (may also middle click on desktop)
• Mod+v – Jump to master windo

    Basic Programs
    • Mod+r – lf (file browser/manager)
    • Mod+R – htop (task manager, system monitor that R*dditors use to look cool)
    • Mod+e – neomutt (email) – Must be first configured by running mw add.
    • Mod+E – abook (contacts, addressbook, emails)
    • Mod+m – ncmpcpp (music player)
    • Mod+w – Web browser (Brave by default)
    • Mod+W – nmtui (for connecting to wireless internet)
    • Mod+n – vimwiki (for notes)
    • Mod+N – newsboat (RSS feed reader)
    • Mod+F4 – pulsemixer (audio system control)
    • Mod+Shift+Enter – Show/hide dropdown terminal
    • Mod+’ – Show/hide dropdown calculator
    • Mod+D – passmenu (password manager)
    Tags/Workspaces

    Tags/Workspaces
    There are nine tags, active tags are highlighted in the top left.
    • Mod+(Number) – Go to that number tag
    • Mod+Shift+(Number) – Send window to that tag
    • Mod+Tab – Go to previous tag (may also use \ for Tab)
    • Mod+g – Go to left tag (hold shift to send window there)
    • Mod+; – Go to right tag (hold shift to send window there)

    Audio
    I use ncmpcpp as a music player, which is a front end for mpd.
    • Mod+m – ncmpcpp, the music player
    • Mod+. – Next track
    • Mod+, – Previous track
    • Mod+< – Restart track
    • Mod+> – Toggle playlist looping
    • Mod+p – Toggle pause
    • Mod+p – Force pause music player daemon and all mpv videos
    • Mod+M – Mute all audio
    • Mod+- – Decrease volume (holding shift increases amount)
    • Mod++ – Increase volume (holding shift increases amount)
    • Mod+[ – Back 10 seconds (holding shift moves by one minute)
    • Mod+] – Forward 10 seconds (holding shift moves by one minute)
    • Mod+F4 – pulsemixer (general audio/volume sink/source control)
    Recording

    Recording
    I use maim and ffmpeg to make different recordings of the desktop and audio. All of these
    recording shortcuts will output into ˜, and will not overwrite previous recordings as their names
