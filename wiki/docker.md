``` txt
File: docker.md
Author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
tags: [tools]
```

#  docker.md intro:

docker.com

Docker is a set of platform as a service products that use OS-level
virtualization to deliver software in packages called containers. Containers
are isolated from one another and bundle their own software, libraries and
configuration files; they can communicate with each other through well-defined
channels.Wikipedia

## voidlinux instalation
+ https://wiki.voidlinux.org/Docker


Installing Docker via XBPS

    xbps-install docker

Installing Docker Compose

Docker Compose is a tool to create and run multi-container Docker apps.

    xbps-install docker-compose

Starting the Docker daemon

Be sure to enable and start the service before trying to use it. If it is not enabled, it won't work.

    ln -s /etc/sv/docker /var/service/

For more documentation about using services, see for example runit.
Post Installation

Add your user to docker group

    doas usermod -aG docker $USER
