---
File: lazygit.md
Author: Sergio Araujo
<<<<<<< HEAD
Last Change:  Tue, 26 Apr 2022 - 13:07:39
=======
Last Change:  Tue, 26 Apr 2022 - 13:07:39
>>>>>>> tmp
Tags: [tools, git, programming]
---

## tool to make easy use git:

    q:       .......................  quit
    p:       .......................  pull
    shift+P: .......................  push

    space:    toggle staged
    c:        commit changes
    shift+C: commit using git editor
    shift+S: stash files
    t:        add patched (i.e. pick chunks of a file to add)
    o:        open
    e:        edit
    s:        open in sublime (requires 'subl' command)
    v:        open in vscode (requires 'code' command)
    i:        add to .gitignore
    d:        delete if untracked checkout if tracked (aka go away)
    shift+R: refresh files

