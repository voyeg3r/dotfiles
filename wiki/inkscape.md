``` txt
file: inkscape.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

# videos pra baixar

[advanced technics](https://design.tutsplus.com/courses/advanced-techniques-in-inkscape)
Inkscape Explained: Path Functions
https://www.youtube.com/watch?v=R8lE2wyfSYY

Tiled clones ->
https://www.youtube.com/watch?v=YZLwVpeu2-g

Every time you create radial tiled clones define:
1 - Move the rotation center off the object
2 - Shift/deslocamento = -100
3 - Rotation/rotação = número de objetos dividido por 360

In order to calculate the angle you can:

    echo $(360/16) | pbcopy

## Shortcuts reference:
+ https://inkscape.org/doc/keys.html

    % ............................ toggle snap

    nodes
    Shift + c ................... make cusp
    Shift + s ................... make smooth
    Shift + y ................... make symetric

    Ctrl + r .................... toggles rules

    Ctrl + Alt + h .............. align by vertical axis
    Ctrl + Alt + t .............. align by horizontal axis
    Ctrl + Alt + 5 .............. align by vertical and horizontal axis

## Select objects (special way):

    Alt + selection tool (to select by touching any objects region)

## shades generator (gradientes)
+ https://noeldelgado.github.io/shadowlord/#ff5233

## Canvas rotate (rotação da página)

    Ctrl+Shift + Scroll

## inkscape 1.0 beta
+ https://inkscape.org/release/1.0beta2/platforms/

    https://inkscape.org/release/inkscape-1.0beta2/gnulinux/appimage/dl/

Compiling the new version

    https://inkscape.org/develop/getting-started/#building-inkscape-linux

## Deleting all guides

    Edit > delete all guides

    Editar > Eliminar todas as guias

## Changing nodes

    Shift + u   ......... make curve
    Shift + l   ......... make line

    Shift + b  .......... break node

## Efeitos em caminhos
+ https://www.youtube.com/watch?v=jqP-NkSeUSc&t=313s

Para criar um efeito de corda crie uma figura que tem duas
extremidades parecidas com a ponta de um facão. (machete)

## Define half size of an object by dividing with slash

When you type the size of the object you can put something like

    90/2  on the size dialog

## Yin Yang logo
+ https://www.youtube.com/watch?v=5ML4MVhvrv4

    Firts combine, then divide

    Another way: Create the intermediate circles, apply union to them,
    select the united form and the big circle and apply division

    OBS: to create a circle with the half of the size use
         Ctrl + Shift + M (transformation)

## Good fonts for desingners

    MiriamLibre https://fonts.google.com/specimen/Miriam+Libre
    http://typeof.net/Iosevka/

## Shortucts list
+ https://inkscape.org/en/doc/keys046.html

    % .................... togle snapping
    Shift - g ............ convert object to guides
    Alt-Click + drag ..... draw a line to select multiple objects

## Import Adobe Ilustrator
+ https://www.youtube.com/watch?v=Hwilw97pR6k

Just change the extension to pdf and vice-versa

## Inkscape open symbols
+ https://github.com/Xaviju/inkscape-open-symbols

## New icon set
+ https://logosbynick.com/new-icons-for-inkscape/
+ http://logosbynick.com/wp-content/uploads/2016/04/Version-92.zip

## How to make inskscape go dark (theme)
+ https://logosbynick.com/how-to-make-inkscape-go-dark/

## How to use templates on inkscape

Put your template files at:

    ~/.config/inkscape/templates

After that you can do:  File > New from template

Tags: inkscape, design
