``` txt
file: adduser.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

# adduser.md Introdução

Adiciona usuários ao sistema

    adduser nome

## usando uma pasta existente como home

    adduser fulano --home=/home/fulano
