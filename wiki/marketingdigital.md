``` txt
File: marketingdigital.md
Author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
Date: jun 17, 2021 - 13:24
tags: [selling, tools, gig]
```

#  marketingdigital.md intro:
+ https://www.youtube.com/watch?v=VAcKBKRmOQY (siglas)

Site com siglas e seus significados
+ https://clictadigital.com/top-100-common-digital-advertising-acronyms-abbreviations/
+ https://www.traycorp.com.br/conteudo/metricas-ads/

## Avaliar popularidade de sites:
+ https://socialblade.com/

Neste site você cola a string de um canal e ele avalia a penetração do mesmo

Termos usados em Marketing Digital:

## SEO - Search Engine Optimization (https://www.tecmundo.com.br/blog/2770-o-que-e-seo-.htm)
Um bom site para gerar longtail keywords: (otimização de buscas não pagas)
+ https://neilpatel.com/br/ubersuggest/

são técnicas utilizadas em sites para serem melhor ranqueados nos mecanismos
de buscas, como o Google. Ou seja, quanto melhor as estratégias adotadas, mais
chances de um site aparecer logo nas primeiras páginas da pesquisa - e também
maior a probabilidade de acessos.

Tem também o SEM (Search Engine Marketing) neste caso está relacionado com
buscas pagas.

## Pay-per-click PPC
+ https://www.wordstream.com/ppc

PPC stands for pay-per-click, a model of internet marketing in which
advertisers pay a fee each time one of their ads is clicked. Essentially, it’s
a way of buying visits to your site, rather than attempting to “earn” those
visits organically.

Search engine advertising is one of the most popular forms of PPC. It allows
advertisers to bid for ad placement in a search engine's sponsored links when
someone searches on a keyword that is related to their business offering. For
example, if we bid on the keyword “PPC software,” our ad might show up in the
very top spot on the Google results page.

## O que é CPC?

+ https://revpanda.com/what-is-cpc-and-cpm-in-digital-marketing/
+ https://www.wordstream.com/ppc

CPC and CPM are some of the most important terminologies of PPC marketing (pay-per-click).
Most of the ads you see on the internet are one of these two. Regardless of
the platform, you use, either social media or search engine platforms, CPC and
CPM are handy acronyms that can help you improve your online revenue.

As one of the most critical terminologies of PPC marketing, CPC is the
abbreviation of “cost per click”. It stands for the amount you are supposed to
pay per each click you get on your ads. In other words, it is the answer to
how much it costs you to acquire a click on a platform.

## What is CTR (click through rate)
 What Is the Click-Through Rate (CTR)?

In online advertising, the click-through rate (CTR) is the percentage of
individuals viewing a web page who view and then click on a specific
advertisement that appears on that page.

## ROI - Return On Investmet

What Is Return on Investment (ROI)?

Return on investment (ROI) is a performance measure used to evaluate the efficiency or profitability of an investment or compare the efficiency of a number of different investments. ROI tries to directly measure the amount of return on a particular investment, relative to the investment’s cost.

## What is Customer Acquisition Cost (CAC)?

Customer acquisition cost (CAC) is the cost related to acquiring a new customer. In other words, CAC refers to the resources and costs incurred to acquire an additional customer.

## What is Google Ads?

Google Ads is the single most popular PPC advertising system in the world. The
Ads platform enables businesses to create ads that appear on Google’s search
engine and other Google properties.

## CRM Customer Relationship Management
É um software de gerenciamento de clientes, com contados, vendas etc.
(Software de gestão de clientes)

## LTKW (Long Tail Keywords)
+ https://ads.google.com/home/tools/keyword-planner/

These are the keywords that have more than 3 words.

## Obter vídeos de graça para republicar no youtube
+ https://www.pexels.com/
+ https://invideo.io/
+ https://www.storyblocks.com/ (it also has a video maker)
    (a good tutorial: https://www.youtube.com/watch?v=2iR54sYiKPY)

OBS: When creating a new video use 1920 x 1080

No youtube, ativando o filtro "creative commons", dá pra achar vídeos que
podemos republicar sem problemas com licenças.

## Plataforma nacional para anúncios (monetizze)
+ https://app.monetizze.com.br/loja

## Biblioteca de músicas do youtube
+ https://www.youtube.com/c/AudioLibraryEN/featured
