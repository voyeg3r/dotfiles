``` txt
File: xbacklight.md
Author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
Date: abr 19, 2020 - 07:43
tags: [tools, desktop]
```

#  xbacklight.md intro:
xbacklight - adjust backlight brightness using RandR extension

    xbacklight -dec 20

see also: xrandr
