``` txt
file: saude.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```


# Arquivo: dicas sobre saude

## O'ring test
+ https://www.youtube.com/watch?v=A9Ir4Exh6uk

O'ring test: conheci o O'ring test em l995, através do sr. Kimio Yamamoto,
pelo qual agradeço com profundo respeito.  É uma metodologia desenvolvida e
patenteada pelo médico professor dr.Yoshiaki Omura, nos  EE.UU. em 1976. deixo
uma fonte de pesquisa no YouTube para pesquisa:BDORT, comprovação clínica no
auxílio do diagnóstico. com a dra. Lucy Harasawa

faz um anel numa mão e segura ou aponta para um alimento ou medicamento,
se for bom para a pessoa você não consegue abrir os dedos, se for ruim você
consegue abrir os dedos.

## Livro: A saúde brota da Natureza

É um livro digital com mais de 500 páginas sobre tratamentos naturais

## como evitar [[ataquecardiaco]]

## cerveja e agua [[cervejaeagua]]

## Curas naturais para gastrite e úlcera

Babosa com água em jejum
Leite de pinhão brabo:
    começa com uma gota por dia até nove
    depois volta a diminuir

## cura natural para pedra nos rins

    água da raiz da malícia
    a folha da malícia é um poderoso anti-inflamatório

## 5 componentes essenciais para a saude
http://www.youtube.com/watch?v=MFp5QLR6xpE&feature=watch-vrec

   Vitamina D  (evita câncer) tomar sol
   Iodo
   Magnésio
   Omega 3 (fonte: berduégua ou belduega)

## Barro para tratamento de picadas peçonhentas

Aplique barro molhado sobre o local, pode ser misturado
com carvão em pó, o que potencializa o seu efeito

## Como provocar febre atificialmente
Coloque um dente de alho embaixo das axilas

A febre mata bactérias no corpo

## Exercícios para a visão

http://www.youtube.com/watch?v=S2Vjib_tGj4&feature=endscreen&NR=1

     Entendimento de que a lente do olho
     é movida por músculos (em geral cansados)

     O primeiro pilar da boa visão é aprender a focar
     O segundo pilar da boa visão é movimentar os olhos
     O terceiro pilar da boa visão é o piscar

     Piscar os olhos naturalmente (5 ou 10 minutos)
     Espalmação (não deixar entrar luz nos olhos por 5 ou 10 minutos)
     Olhar solar (ficar de frente para o sol com os olhos fechados)
     Relaxamento da ioga balançando os braços
     Leitura de um livro de cabeça para baixo (focar a visão)

## agua alcalina

A água alcalina não é absorvida no estado ALCALINO pelo organismo, pois quando
ela atinge o estômago, que é um ambiente ácido, ela se acidifica. Mas
é justamente na ACIDIFICAÇÃO da água com PH elevado que ocorre a grande
contribuição de se ingerir água alcalina. Toda vez que o estômago é atingido,
digamos, pela água com pH alto, o pH do estômago vai naturalmente se elevar num
primeiro momento. O corpo vai imediatamente responder ao “erro” produzindo
ácido clorídrico para corrigir o pH do estômago. Acontece que por um mecanismo
de compensação, o sistema que produz ácido clorídrico dispara um gatilho que
faz com que o pâncreas produza bicarbonato de sódio, cujo pH é acima de oito,
super alcalino, e esse bicarbonato entra direto no plasma sanguíneo,
ALCALINIZANDO todo o sistema. Como água não produz nenhum tipo de resíduo, e,
diga-se de passagem, todo resíduo tem no final uma natureza acídica quando
entra na corrente sanguínea, essa produção digamos extra de bicarbonato de
sódio fica a crédito em um sistema que após os 45 anos mais ou menos do
indivíduo, começa a diminuir, aumentando a velocidade do envelhecimento.


