``` txt
File: python-libraries.md
Author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
Date: mar 03, 2021 - 18:10
tags: [tags]
```

#  python-libraries.md intro:

+ [pathlib](pathlib.md) - manipula paths do sistema
+ [python-magic](python-magic.md) - determina tipo mime
+ [click](click.md)   - criação de interfaces CLI
+ random  - geração de números randômicos
+ [google](googlesearch.md)   pip install --user google
+ [regex-rename](regex-rename.md) pip3 install --user regex-rename
+ [py-rename](py-rename.md)

# vim: ft=markdown et sw=4 ts=4 cole=0
