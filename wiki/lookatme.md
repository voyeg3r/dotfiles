```txt
 Last Change:  Wed, 22 Jun 2022 - 19:42:58
 Filename: lookatme.md
 tags: [spreedsheet, tools, ppt, nvim, markdown]
```

## lookatme is an interactive, extensible, terminal-based markdown presentation tool.
+ https://github.com/d0c-s4vage/lookatme
+ https://github.com/waylonwalker/lookatme
