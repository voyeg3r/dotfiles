``` txt
file: cd.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

# cd - mudar de diretórios

    cd -    ............ voltar ao último diretório
    cd ..   ............ subir um nível
	cd      ............ quando digitado sem argumentos volta ao $HOME

Pra quem usa o `zsh` podemos fazer coisas assim:

		cd =python3(:h)
