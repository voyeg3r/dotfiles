``` txt
file: feedly.md
author: Sergio Araujo
Last Change: Mon, 01 Aug 2022 14:20:13
tags: [rss, news]
```

## feedly is an online css client

## How to change my passowrd?
+ https://feedly.com/i/account/logins

## How to export feedly opml file?
+ https://zapier.com/blog/feedly-export-opml/

    prefrencies →  then click on the arrow

## feedly shortcuts

     ┌─────────────┬─────────────────────────────────┐
     │     j       │  Next article                   │
     ├─────────────┼─────────────────────────────────┤
     │     K       │  Previous article               │
     ├─────────────┼─────────────────────────────────┤
     │     v       │  View origial in a new tab      │
     ├─────────────┼─────────────────────────────────┤
     │     gl      │  Read later                     │
     ├─────────────┼─────────────────────────────────┤
     │     ?       │  Show help                      │
     └─────────────┴─────────────────────────────────┘

<!-- vim:ft=markdown:et:sw=4:ts=4:cole=0:-->
