```txt
Filename: wgetpaste.md
Last Change: Sun, 21 Aug 2022 13:04:27
tags: [tools, snippets]
```

## paste snippets in various sites:
+ https://ubunlog.com/en/wgetpaste-fragmentos-codigo-terminal/#Carga_fragmentos_de_texto_con_Wgetpaste

## Discover services:

```sh
wgetpaste -S
```

## Paste command + command output
```sh
wgetpaste -c 'pwd'
```

## Set language:

```sh
wgetpaste -l Bash mi-texto.txt
```



