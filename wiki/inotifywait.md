``` txt
File: inotifywait.md
Author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
Date: nov 21, 2020 - 20:48
tags: [tools]
```

#  inotifywait.md intro:

    inotifywait - wait for changes to files using inotify

## Installation on (voidlinux)

    sudo xbps-install -S inotify-tools

