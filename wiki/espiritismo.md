``` txt
file: espiritismo.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

Mantenha o seu voto de silêncio

Atenha-se exclusivamente às suas tarefas para fazê-las bem feito. Eu só vou
ficar aquí o tempo que a espiritualidade achar conveninente, eu não fiz
planos.

As tarefas que a espiritualidade determinar eu tentarei dentro das minhas
limitações executar.

Tipo: Você está morando aqui?
Não: Não exatamente morando, estou aqui pelo tempo que a espiritualidade
determinar para executar algumas tarefas que
precisam de cuidados diários, serviços como cuidar do galinheiro, fazer a
coleta do lixo do entorno etc. Obviamente que se a coordenação determinar
outros afazeres me sentirei feliz em poder contribuir de algum modo.

----------------------------------

 Mohib Ibn Árabe, formulador do islã místico, conhecido como mestre
 dos mestres, foi, talvez, quem melhor traduziu a religião do amor
 quando dizia, há cerca de 700 anos:

“Meu coração se tornou capaz de todas as coisas: é pastagem para as
gazelas, convento para os monges e templo para os ídolos; é a Caaba
para os peregrinos, a tábua das leis e o livro do Corão. Minha
religião é a religião do amor. Seja qual for o caminho que os camelos
tomem, esta é minha religião e minha fé”.

fonte: http://www.smashwords.com/extreader/read/11419/9/terra-a-ilha-da-fantasia

[previna se](previna-se.md)
[Prece da Gratidão](prece-da-gratidao.md)

## Numerologia

    Códigos que você deve usar

    Ansiedade ............................ 51949131948
    Harmonizar o presente ................ 71042
    Dinheiro Inesperado .................. 520
    Tudo é Possível ...................... 5197148
    Tudo Ficará 100% ..................... 918197185
    Solução Imediata ..................... 741
    Prosperidade ......................... 318798

    Lembre-se de recitar os números um por um
    Ex: Sete, quatro, um

## Maual de viagem astral (viagemastral.com)
+ (capítulo 1) https://www.youtube.com/watch?v=sG2p2FpmRNU
+ (capítulo 2) https://www.youtube.com/watch?v=NTKu-gaD9Hk

## Palestras Espíritas
+ A mente extrafísica - https://www.youtube.com/watch?v=s7eG0t408s4
+ Tudo é pensameto - https://www.youtube.com/watch?v=R4G9DWwIn9E
+ O espírito me disse - https://www.youtube.com/watch?v=4O0KSCoSjyU
