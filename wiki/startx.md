``` txt
file: startx.md
author: Sergio Araujo
Last Change: Mon, 12 Sep 2022 16:54:22
tags: [x11, desktop]
```

# Introdução
Para abrir outro modo gráfico

    startx -- :1

inicia no display 1 (ctrl+alt+F8)
no meu caso usei (ctrl+alt+F9)

Iniciando uma segunda seção do dwm:

    Ctrl+Alt+F2
    cd ~/.dotfiles/dwm
    startx ./dwm

Usando o método acima eu posso testar configurações do dwm em uma
segunda seção gráfica

