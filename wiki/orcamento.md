``` txt
File: orcamento.md
Author: Sergio Araujo
Last Change:  Sat, 16 Jul 2022 - 20:06:03
tags: [vim,nvim]
```

#  orcamento.md:

## Como somar

Selecione os números e substitua \n por +

    :put =substitute(@0, '\n', '+', 'g')
    Ctrl-r =  Ctrl-r 0

    ------------------
    Abril de 2020
    ------------------
    mosquiteiro     20
    energia         50
    internet        30
    armário        250
    crediamigo      30
    ------------------
    Total:         380

