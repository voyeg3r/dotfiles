```txt
 Last Change:  Tue, 26 Apr 2022 - 13:07:39
 Filename: fortune.md
```

## print a random, hopefully interesting, adage

algumas opções que podem ser utilizadas com o Fortune:
–a Indica que o fortune seja selecionado de entre todas as bases de dados.
–n 100 Indica que seja imprimido um fortune com o número de carateres indicado.
–o Indica que o fortune a imprimir seja selecionado da base de dados de citações ofensivas.
–s Indica que seja selecionado um fortune de uma frase apenas.
