``` txt
file: df.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

# Introdução
Exibe o espaço livre 'disck free' - disk usage

    df -H

O comando acima gera uma saida mais ou menos assim:

    Filesystem     Size   Used   Avail  Use%  Mouted on
    /dev/sda1      9,8G   3,8G    5,5G   41%  /

A combinação de comandos permite coisas do tipo

    PENDRIVE=$(df |grep sdb|awk '{print $NF}')

tags: tools, admin, disk
