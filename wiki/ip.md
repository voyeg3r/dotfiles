``` txt
file: ip.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

# ip.md

O ip é utilizado para atribuir endereços IP para as interfaces de rede,
mostrar configuração do TCP/IP, entre outras coisas.

O comando abaixo mostra as rotas ativas

## adicionar rota

    ip route add to 0.0.0.0/0 via 192.168.1.1
    ip route show

    ip addr show

## pegar o ip público

    dig +short myip.opendns.com @resolver1.opendns.com

## pegar o ip atual

    ip addr

    alias localip="ip addr | grep -Po '(?<=inet)\s(?!127)[^/]+'"

## Mostrar o gateway padrão

    awk '/default/ {print $3}' <(ip route show)
   	awk 'NR==1 {print $3}' <(ip route show)

## identificar placas de rede

    ip link show

## Show ip address

    ip addr | awk '/global/ {print $2}'
    192.168.0.122/24

    awk '/global/ {print $2}' <(ip addr)
    192.168.1.6/24

## if you don't have network configurated try

    ip link

    ip link set "interface" up
    dhcpd "interface"

tags: [network, tools]
