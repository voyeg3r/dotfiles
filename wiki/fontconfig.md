```txt
File: /home/sergio/.dotfiles/wiki/fontconfig.md
Last Change:  Thu, 12 May 2022 - 13:57:37
tags: [fonts, utils, fc-list, fc-cache]
```

## Fontconfig provides fc-list and fc-cache

## Searching for a specific ChangeFont

    fc-list | grep -i "hack"

In order to set font in this way

    st -f 'Hack-Regular:size=15'


## just show font names:

    fc-list : family
