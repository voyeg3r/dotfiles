``` txt
file: ntpdate.md
author: Sergio Araujo
Last Change: Sat, 17 Sep 2022 03:41:48
tags: [date, ntp, tools]
```

## ntpdate

### testar se o serviço está ok

No caso do voidlinux estou usando o openntpd e a chamada do programa
difere do ntpd

    openntpd -n
    configuration OK

    ou

    ntpd -n

Escolheremos como fornecedor de horário confiável o seguintes servidores:

       0.br.pool.ntp.org
	   1.br.pool.ntp.org
	   2.br.pool.ntp.org
	   3.br.pool.ntp.org

    doas ntpdate -u 0.br.pool.ntp.org

## usando o cron para ativar o ntpdate
   manter o relogio certo

    0 * * * * /sbin/ntpdate -s 0.br.pool.ntp.org

## minuto hora dia mês dia da semana comando

