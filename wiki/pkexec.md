``` txt
file: pkexec.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```


# pkexec

pkexec - Execute a command as another user

    pkexec='pkexec env DISPLAY=$DISPLAY XAUTHORITY=$XAUTHORITY'
