``` txt
file: printf.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```


Imprimir uma sequência de 1 a 300 usando 3 dígitos

## Bash 3

   printf "%03d\n" {1..300}

   printf "%s\n" {01..10}

   Quando usamos %s designamos string
   Quando usando %d atribuimos decimais

