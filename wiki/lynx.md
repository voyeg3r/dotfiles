``` txt
file: lynx.md
author: Sergio Araujo
Last Change:  Thu, 21 Jul 2022 - 12:58:21
```

# change lynx settings

+ https://superuser.com/a/518451/45032
+ https://unix.stackexchange.com/q/361696/3157

    lynx.cfg ........... lynx config file
    alias lynx='lynx -cfg=~/.lynx.cfg'

This way you can set some important changes

    alias lynx='lynx -accept_all_cookies'
    CHARACTER_SET:UNICODE (UTF-8)

## get page links

    lynx -dump -hiddenlinks=listonly link

## pegar todos os links de uma p�gina

    lynx -dump http://www.domain.com | awk '/http/{print $2}'

    lynx -dump url > filetext.txt

    lynx -dump -display_charset=utf-8 site | less

## Dump of local file

    lynx --dump ./local-file > final-file

## Changing user-agent

		lynx -useragent="Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_0) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.79 Safari/537.1"

## Enviando torpedo claro
+ http://bit.ly/2USmcjB

```bash
lynx -dump -accept_all_cookies "http://www2.claro.com.br/torpedoweb/popup_senderJava.asp?ddd_para=88&telefone_para=992488666&nome_de=seulove &ddd_de=88&telefone_de=992488666&msg=Just+a+test!&caract=98"

lynx -dump -accept_all_cookies 'http://www2.claro.com.br/torpedoweb/popup_senderJava.asp?\ ddd_para=17&telefone_para=810000&nome_de=Cabelo&ddd_de=&\ telefone_de=&msg=Prezado+FABIO:+Viva+o+Linux!&caract=98'
```

```bash
dest=992488666
from=992488666
ddd_dest=88
ddd_from=88
name=seulove

lynx -dump -accept_all_cookies "http://www2.claro.com.br/torpedoweb/popup_senderJava.asp?ddd_para=$ddd_dest&telefone_para=$dest&nome_de=$name&ddd_de=$ddd_from&telefone_de=$from&msg=Just+a+test!&caract=98"
```
