``` txt
file: xterm.md
author: Sergio Araujo
Last Change: Sat, 17 Sep 2022 11:02:37
tags: [terminal, cli]
```

## Config xterm

    alias xterm='xterm -fn 7x13 -fa "Liberation Mono:size=14:antialias=false"'

    xterm -fn 7x13 -fa "IBMPlexMono-Regular:size=12:antialias=true"

## xterm shortcuts

    ^H .............. erases til the begining of line
    ^w .............. erases one word

## Using xterm with fzf as a fuzzy app laucher

    xterm -name 'floating xterm' -e "fzf $* < /proc/$$/fd/0 > /proc/$$/fd/1"

