``` txt
File: openntpd.md
Author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
tags: [tools, network]
```

#  openntpd.md intro:
+ https://www.openntpd.org/

OpenNTPD is a Unix daemon implementing the Network Time Protocol to synchronize
the local clock of a computer system with remote NTP servers. It is also able
to act as an NTP server to NTP-compatible clients.

    doas xbps-install -Sy openntpd
    ln -s /etc/sv/openntpd /var/service/openntpd
