``` txt
file: exiftool.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

# exiftool - edit images and images metadata - sex 11 mai 2018 15:26:43 -03
+ https://hvdwolf.github.io/pyExifToolGUI/

    exiftool - Read and write meta information in files

## Pegar a data de criação de uma foto
+ https://aurelio.net/blog/2010/05/15/datas-fotos-e-shell/

    exiftool img.jpg | grep 'Create Date' | head -1 | cut -d: -f2-

Para modificar a data usamos o comando touch (usando a opção -t)

    exiftool NF | grep 'Create Date' | head -1 | cut -d: -f2- | tr -d ': ' | sed 's/./&./12'

``` markdown
for i in *; do
echo touch -t $(exiftool NF | grep 'Create Date' | head -1 | cut -d: -f2- | tr -d ': ' | sed 's/./&./12'\n) $i
done
```

## Change pdf title:
+ https://apple.stackexchange.com/a/274742/303151

    exiftool -Title="New Title" /path/to/filename.pdf

## Substitui palavras-chave existentes por duas novas palavras chaves.

    exiftool -keywords=palavra1 -keywords=palavra2 foto.jpg

## Remove all metadata from a file:

    exiftool -all= -overwrite_original "file name.extension"

## Remove all metadata from the current directory:

    exiftool -all= -overwrite_original .

## Remove all metadata from all png files in the working directory:

    exiftool -all= -overwrite_original -ext png .
