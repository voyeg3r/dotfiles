``` txt
file: killall.md
author: Sergio Araujo
Last Change: Mon, 08 Aug 2022 11:29:02
tags: [admin, tools, kill, ps]
```

# Mata todos os processos

    killall -9 /usr/local/netscape/mozilla-bin
