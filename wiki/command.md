```txt
Filename: command.md
Last Change: Thu, 11 Aug 2022 12:24:50
tags: [bash, shell, cmd, builtin]
```

## Test if a command exists:
+ https://stackoverflow.com/a/677212/2571881

```sh
if ! command -v <the_command> &> /dev/null
then
    echo "<the_command> could not be found"
    exit
fi
```


