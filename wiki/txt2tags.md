``` txt
File: txt2tags.md
Author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
Date: out 04, 2020 - 09:53
tags: [tools]
```

#  txt2tags.md intro:

Txt2tags is a document generator created by Aurelio Jargas in 2001. It reads a
text file with minimal markup such as **bold** and //italic// and converts it
to many formats, such as:

    AsciiDoc
    DocBook
    HTML
    LaTeX
    MoinMoin
    UNIX man page
    Wikipedia/MediaWiki
    …and many others.
