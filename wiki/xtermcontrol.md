``` txt
file: xtermcontrol.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

## get terminal bg:

    xtermcontrol --get-bg
