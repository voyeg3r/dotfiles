```txt
-- File: /home/sergio/.dotfiles/wiki/which-key.md
-- Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

## Intro:

WhichKey is a lua plugin for Neovim 0.5 that displays a popup with possible key bindings of the command you started typing. Heavily inspired by the original emacs-which-key and vim-which-key.

- https://github.com/folke/which-key.nvim


If you type your `<leader> + key` fast enough, which depends on your
`vim.opt.timeoutlen` variable (see lua/options.lua), which-key will not show
the keybindings/shortcuts. Notice! Not every keybinding is listed on which-key,
but the config file under ./lua/user/whichkey.lua has groups like this:

```lua
    p = {
        name = "Packer",
        c = { "<cmd>PackerCompile<cr>", "Compile" },
        i = { "<cmd>PackerInstall<cr>", "Install" },
        s = { "<cmd>PackerSync<cr>", "Sync" },
        S = { "<cmd>PackerStatus<cr>", "Status" },
        u = { "<cmd>PackerUpdate<cr>", "Update" },
    },
```
