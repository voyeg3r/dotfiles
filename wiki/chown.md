``` txt
file: chown.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

# Introdução
Modifica o dono de arquivos e pastas (propriedade)

    chown -R user:user /home/user

    chown -v greys:greys try
    changed ownership of 'try' from root:root to greys:greys

