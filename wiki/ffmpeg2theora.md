``` txt
file: ffmpeg2theora.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

# Converter video flv para ogv

ffmpeg2theora -V 200 -A 64 test.flv

## Converter ogv para flv

ffmpeg -i input.ogv -ar 44100 -acodec libmp3lame output.flv

