``` txt
file: csplit.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

csplit -z -f split teste '/evento/' '{*}'

csplit prog.c '/^}/+1' {100}

## splits prog.c into different
## routines where it is assumed
## that each routine ends with a
## '}' at the start of a line

