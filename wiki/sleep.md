``` txt
file: sleep.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```


O comando sleep por padrão trabalha com segundos


sleep 5; echo "olá mundo"


Mas você pode usar horas

sleep 1h; echo "olá mundo"
