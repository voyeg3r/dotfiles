```txt
Filename: ventoy.md
Last Change: Mon, 01 Aug 2022 14:43:28
tags: [usb, stick, iso, tools, liveusb]
```

## site:
+ https://www.ventoy.net/en/index.html
+ https://www.youtube.com/watch?v=NOZmSTT7jKM
+ https://youtu.be/11CkqZQ3scE (video do diolinux)
+ https://www.hardware.com.br/comunidade/v-t/1538171/

Download ventoy [here](https://github.com/ventoy/Ventoy/releases)
then unpack the file, enter the folder and run:

    sudo Ventoy2Disk.sh { -i | -I | -u } /dev/sdX

If you do not know your usb device address run:

    lsblk
    sudo lsblk -o model,name,fstype,size,label,mountpoint

or

    doas blkid


