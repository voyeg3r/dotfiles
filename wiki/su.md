``` txt
file: su.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```


# intro

to become admin you should use this command
+ https://www.tecmint.com/difference-between-su-and-su-commands-in-linux/

    sudo su -

user is provided his own default login environment,
including path to executable files;
he also lands into his default home directory.

To become another regular user

    su uer



