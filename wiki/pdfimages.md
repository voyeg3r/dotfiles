``` txt
file: pdfimages.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```


# pdfimages - dom 01 abr 2018 12:12:02 -03

## Extract images from a pdf file

    pdfimages -j file.pdf target-folder

