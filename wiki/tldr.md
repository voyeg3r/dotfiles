---
File: tldr.md
Author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
tags: [tools, cli, man]
---

## Shorter and better man pages

    tldr find

## update tldr database

    tldr -u
