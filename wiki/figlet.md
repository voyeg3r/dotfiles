``` txt
file: figlet.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
abstrac: This aims...
```

#  figlet.md intro:

display large characteres made up of ordinary screen characters

## On vim

    :r !figlet demo

        _
     __| | ___ _ __ ___   ___
    / _` |/ _ \ '_ ` _ \ / _ \
   | (_| |  __/ | | | | | (_) |
    \__,_|\___|_| |_| |_|\___/

To center the text use `-c` flag

tags: art, design, asciiart
