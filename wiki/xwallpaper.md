``` txt
File: xwallpaper.md
Author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
tags: [tools, wallpaper, desktop]
```

#  xwallpaper.md intro:
wallpaper setting utility for x

    xwallpaper --zoom path/to/your/wallpaper.jpg

