``` txt
file: html2text.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

# pegando seu ip externo

		html2text http://checkip.dyndns.org | grep -i 'Current IP Address:'|cut -d' ' -f4

## veja também
[[ifconfig]]
