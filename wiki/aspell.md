``` txt
file: aspell.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

#  aspell.md intro:

To fix spell mistakes on \LaTeX\ documents just type

    aspell -t book.tex
