``` txt
file: fold.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

Utilizando o fold a saída será devidamente formatada para uma “tela” de, no
caso, 80 colunas de largura.

    fold --width=70 --spaces lorem_ipsum.txt

