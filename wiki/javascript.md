```txt
javascript.md
last change: Sun, 26 Dec 2021 11:25
```

JSON ( JavaScript Object Notation, pronounced / ˈdʒeɪsən /; also / ˈdʒeɪˌsɒn /) is an open standard file format and data
interchange format that uses human-readable text to store and transmit data objects consisting of attribute–value pairs
and arrays (or other serializable values).

- https://docs.npmjs.com/creating-a-package-json-file
