``` txt
file: gundoplugin.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

# introdução

http://sjl.bitbucket.org/gundo.vim/

screencast: http://screenr.com/M9l

## instalação

Para quem usa o vim-plug

    hg clone http://bitbucket.org/sjl/gundo.vim ~/.config/nvim/plugged/gundo

Add a mapping to your ~/.vimrc (change the key to suit your taste):

    nnoremap <F5> :GundoToggle<CR>

tag: vim, nvim
