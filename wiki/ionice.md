``` txt
file: ionice.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

# ionice.md - set or get process I/O scheduling class and priority
+ https://www.tecmint.com/delete-huge-files-in-linux/

    Last Change: mar 19 2019 08:11

## Deleting Huge Files in Linux

    ionice -c 3 rm /var/logs/syslog
    ionice -c 3 rm -rf /var/log/apache

