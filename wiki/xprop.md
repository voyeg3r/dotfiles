```txt
File: /home/sergio/.dotfiles/wiki/xprop.md
Last Change: Tue, 02 Aug 2022 10:28:11
tags: [tools, desktop, x11]
```

## xprop - property displayer for X

Get the app name:

The xprop utility is for displaying window and font properties
in an X server.  One window or font is selected using the
command line arguments or possibly in the case of a window, by
clicking on the desired window.  A list of properties is then
given, possibly with formatting information.

    xprop | awk '/WM_CLASS/ {print $4}'



