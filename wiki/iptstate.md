``` txt
file: iptstate.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

Exibir coneções pela porta 443 vindas do ip 192.168.0.100

iptstate -s 192.168.0.100 -D 443 -l

link sobre como bloquear o ultra surf
* http://www.opcaolinux.com.br/gnulinux/tutoriais/27-seguranca/121-ultrasurf-entendendo-e-combatendo-o-inimigo.html

link para download
* https://launchpad.net/ubuntu/+source/iptstate
