``` txt
file: xbmc.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```



    Atalhos do Xbmc

    P.......................Play
    Espaço..................Pausa
    X.......................Stop
    L.......................Ativar/DesativarLegendas
    F.......................Adiantar(FastForward)
    R.......................Retroceder(Rewind)
    Setapracima.............Acima(Up)
    Setapraesquerda.........Esquerda(Left)
    Setapradireita..........Direita(Right)
    Setaprabaixo............Abaixo(Down)
    Enter...................Seleciona
    PageUp..................Avançavárioscamposafrente
    PageDown................Retrocedevárioscampos
    BackSpace(-)............Voltaumnívelacima(ParentDirectory)
    Q.......................AdicionaMúsicaouVídeoàFILAdeReprodução
    O.......................ExibeousodoCPUeinformaçõesdediagnósticodevídeo
    W.......................MarcaoVídeocomoassistido
    M.......................AtivaoMenudeControle(OSDPlayerControls)
    S.......................AtivaoMenudeDesligamento(ShutdownMenu)
    Esc.....................HomeMenu(RetornaaoMenuPrincipal)
    I.......................InformaçõesdaMídia
    C.......................MenudeContexto
    .(ponto)................ReproduzirPróximo
    ,(vírgula)..............ReproduzirAnterior
    TAB.....................AlternaentreosmodosdeTelaCheia(ouvisualizaçãooureproduçãodevídeo)
    PrintScreen.............Screenshot(fotodatela)
    Menos(-)................AbaixaVolume
    Mais(+).................AumentaVolume
    BarraInvertida(\).......AlternaoXBMCentreTelaCheiaeMododeJanela
    End.....................DesligaoXBMC(ShutdownXBMC)
