``` txt
file: repeatplugin.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```


# Para colocar uma serie de palavras entre aspas

   csw" ................ coloca palavra atual entre aspas
   . ................... repete a inserção das aspas

   ( necessita do plugin repeat instalado )


tags: vim, nvim
