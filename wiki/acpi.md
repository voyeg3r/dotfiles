``` txt
file: acpi.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

# acpi - verify battery status

    acpi
    Battery 0: Discharging, 85%, 00:32:10 remaining

## verifing your battery at each 5 seconds

    watch --interval=5 acpi -V

tag: hardware, admin, adm
