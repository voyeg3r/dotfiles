``` txt
file: weasyprint.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
tags: [html2pdf, htmltopdf, pdf, convert, tools, python3]
```

# weasyprint (python module)
+ https://weasyprint.org/

    sudo pip3 install easyprint

## How to use it?
+ https://weasyprint.readthedocs.io/en/stable/

    weasyprint https://weasyprint.org/ weasyprint.pdf

