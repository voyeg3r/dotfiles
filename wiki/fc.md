``` txt
file: fc.md
Author: Sergio Araujo
Last Change:  Tue, 31 May 2022 - 12:06:23
tags: [utils, cli, commands]
```

## fc — process the command history list

O comando fc edita o último comando digitado mas também pode receber um parâmetro

    fc grep

## Edit last command

    fc

## Para listar os últimos 5 comandos

    fc -l -5

## Listar os últimos 16 comandos

    fc -l

## Editar comando longo no editor padrão

    ^x ^e

