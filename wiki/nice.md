``` txt
file: nice.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

# nice - run a program with modified scheduling priority

See also [[renice.md]]

 A sintaxe do comando é simples:

   #> nice -n <PRIORIDADE> COMANDO [ARGUMENTOS]

 Sendo que:

<PRIORIDADE>: Refere-se ao número citado acima, de -20 a 20 para o root, de 0
a 20 para usuários normais;

COMANDO: É o comando ao qual a <PRIORIDADE> será aplicada.

[ARGUMENTOS]: São os argumentos do COMANDO, caso sejam necessários.

Após executado, você pode verificar a prioridade de seu comando digitando:

   #> top

