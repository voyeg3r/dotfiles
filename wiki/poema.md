``` txt
Quando meus olhos encontram os teus senti como se eles estivessem
perdidos há muito e finalmente se encontrado, como se fossemos um,
como se um pedaço de mim, que houvera se separado, voltasse.
Last Change:  Tue, 26 Apr 2022 - 13:07:39


Senti pela primeira vez uma senção que nunca havia antes sentido, já houvera
olhado outros olhos, mas nunca nenhum teve tanta força, tanta energia, tanta
reciprocidade. Aquilo me tomou por inteiro, e eu queria mais, e quero a todo
instante.

Agora mesmo distante, vejo esses olhos me olhando, aqui nos
recondidos da mente, e fico a imagicar quando os terei de novo
a me deliciar. Assim queria estar perto de tí mais e mais, pra
cada vez que der vontade, olhar no teus olhos, tão doces, tão meus.

