``` txt
file: cal.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

# cal.md

Comando calendario

## Exibir o calendário do mês corrente

		cal -m -1

Por padrão cal exibe o mês corrente
