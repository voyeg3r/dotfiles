``` txt
File: evince.md
Author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
Date: nov 21, 2020 - 20:35
tags: [tags]
```

#  evince.md intro:

Open evince at page number

    evince -i 10 book.pdf

## Invert colors
+ https://askubuntu.com/a/191582/3798

    Ctrl + i

