``` txt
File: bitbucket.md
Author: Sergio Araujo
Last Change: Sat, 17 Sep 2022 11:00:16
Date: fev 05, 2021 - 22:56
tags: [git, tools]
```

#  bitbucket.md intro:

## my snippets
+ https://bitbucket.org/dashboard/snippets

## Downloading raw files from bitbucket

    wget -c https://bitbucket.org/sergio/dotfaster/raw/master/inputrc -O ~/.inputrc
    wget -c https://bitbucket.org/sergio/dotfaster/raw/master/algorithm/shell/bash/settermux.sh -O settermux.sh

    # the pattern is:
    https://bitbucket.org/<username>/<repo-name>/raw/<branch>/<file-name>
