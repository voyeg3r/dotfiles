```txt
Filename: nvim-frameworks.md
Last Change: Thu, 11 Aug 2022 11:08:55
tags: [nvim, frameworks]
```
## NvChad
+ [nvchad](https://github.com/NvChad)

## Neovim Essentials
+ [NeoVim_Essentials](https://github.com/Blovio/NeoVim_Essentials/)

A nice video about nvim configuration: https://www.youtube.com/watch?v=ajmK0ZNcM4Q

