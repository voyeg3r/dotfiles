``` txt
file: which.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

# O comando which é usado para localizar comandos

which python
/usr/bin/python

## Getting a function definition

    which mkcd

output

    mkcd () {
        mkdir -pv -p "$@" && cd $_
    }

Another option

    declare -f mkcd

    mkcd () {
        mkdir -pv -p "$@" && cd $_
    }
