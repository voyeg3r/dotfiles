```txt
Fname: /home/sergio/.dotfiles/wiki/lsp.md
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

## install sumneko_lua:

The Language Server Protocol is an open, JSON-RPC-based protocol for use between source code editors or integrated development environments and servers that provide programming language-specific features. The goal of the protocol is to allow programming language support to be implemented and distributed independently of any given editor or IDE.

```sh
# https://jdhao.github.io/2021/08/12/nvim_sumneko_lua_conf/
# in my case I have cloned to my .dotfiles
cd $HOME/.dotfiles
git clone --depth=1 https://github.com/sumneko/lua-language-server.git

cd lua-language-server
# if the cloning speed is too slow, edit .gitmodules and replace github.com
# with hub.fastgit.org, which should be faster than github.
git submodule update --init --recursive

# build on Linux
cd 3rd/luamake
compile/install.sh
cd ../..
./3rd/luamake/luamake rebuild
```

## setting up sumneko_lua

On your zsh/bash rc

```sh
[[ -d ${HOME}/.dotfiles/lua-language-server ]] && export PATH=${HOME}/.dotfiles/lua-language-server/bin/lua-language-server:${PATH}
```

```lua
local nvim_lsp = require('lspcofig')

local servers = { "rust_analyzer", "sumneko_lua", "pyright", "tsserver" }
for _, lsp in ipairs(servers) do
    nvim_lsp[lsp].setup({
        on_attach = on_attach,
        capabilities = capabilities,
    })
end
```
