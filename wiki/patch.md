File: /home/sergio/.dotfiles/wiki/patch.md
+ https://linuxhint.com/run-patch-command-in-linux/

patch - apply a diff file to an original

    patch -Np1 < patches/patch-name

## How to remove a patch:

    patch -R < /patches/patch-name
