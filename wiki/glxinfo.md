```txt
File: /home/sergio/.dotfiles/wiki/glxinfo.md
Last Change:  Tue, 26 Apr 2022 - 13:07:39
tags: [screen, monitor, video, tools]
```

glxinfo is a command-line tool that can help you diagnose problems with your 3D acceleration setup. It can also provide additional helpful information for developers. Use man glxinfo to get an overview of its options.

    glxinfo -B
