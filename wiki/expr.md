``` txt
file: expr.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

O comando **expr** pode ser usado para fazer cálculos

expr 33 + 1220
1253

Para fazer uma multiplicação protegemos o asterisco (sinal de multiplicação)
com uma barra invertida

expr 5 \* 10
50

fonte: http://www.vivaolinux.com.br/dica/Utilizando-o-comando-expr

