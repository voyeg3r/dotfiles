``` txt
file: regex.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```


# This page has some tips about Regular Expressions
* [regex - best practices](http://www.softpanorama.org/Scripting/Perlbook/Ch05/best_practices.shtml)
* [vim regexes are awesome](vim-regexes-are-awesome.md)
* [another vim regexes article](http://andrewradev.com/2011/05/08/vim-regexes/)
* [vim regexes](vim-regexes.md)

## See also [dicasvim](dicasvim.md) to learn about regex on vim

