```txt
Filename: snippy.md
Last Change: Tue, 16 Aug 2022 12:59:53
tags: [nvim, snippets]
```

## A snippets plugin for Neovim 0.5.0+ written in Lua.
+ https://github.com/dcampos/nvim-snippy

See also https://github.com/dcampos/cmp-snippy
nvim-snippy completion source for nvim-cmp.
