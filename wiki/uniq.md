``` txt
file: uniq.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```


# Retira duplicados e mostra contagem do que estava duplicado

sort namesd.txt | uniq –c

## Exibir somente as entradas duplicadas

sort namesd.txt | uniq –cd




