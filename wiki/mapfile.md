``` txt
file: mapfile.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

# bash (builtin) lê um arquivo e gera um vetor
+ http://www.dicas-l.com.br/cantinhodoshell/cantinhodoshell_20100121.php

``` bash
$ cat frutas
abacate
maçã
morango
pera
tangerina
uva

$ mapfile vet < frutas  # Mandando frutas para vetor vet
$ echo ${vet[@]}        # Listando todos elementos de vet
abacate maçã morango pera tangerina uva
```

Obteríamos resultado idêntico se fizéssemos:

``` bash
$ vet=($(cat frutas))
```
