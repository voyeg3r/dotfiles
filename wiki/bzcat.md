``` txt
file: bzcat.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

# bzcat.md

		bzcat - decompresses files to stdout

## Example
+ https://stackoverflow.com/a/47226061/2571881

		for i in *; do bzcat "$i" | head -n10; done

