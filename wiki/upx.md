```txt
Filename: upx.md
Last Change: Sun, 31 Jul 2022 08:04:06
tags: [tools, admin, compress]
```

## upx:
+ https://www.hardware.com.br/dicas/comprimindo-executaveis-upx.html

O programa upx comprime executáveis em até 70%

    doas upx -9 -v /bin/nvim



# vim:cole=0:
