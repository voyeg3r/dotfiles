``` txt
File: printenv.md
Author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
Date: abr 20, 2020 - 08:40
tags: [tools, environment]
```

#  printenv.md intro:
+ https://bit.ly/3ammLXg

show environment settings

    printenv MANPAGER
    vim -M +MANPAGER -

Another way of seeing environment settings is using the command

    env
