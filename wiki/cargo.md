---
File: cargo.md
Author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
tags: [rust, tools]
---

#  Rust package manager

On voidlinux run:

    doas xbps-install -Sy cargo

Using cargo you can install `tree-sitter-cli`

    cargo install tree-siter-cli

