``` txt
file: lame.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

## converter wav para mp3

    lame -V2 rec01.wav rec01.mp3

## converter mp4 para mp3

    faad -o - "$i" | lame -h -b 192 - "${i%m4a}mp3"

## Put all converted in the same directory:

    find sounddir -type f -name '*.wav' | \
        parallel lame {} -o mydir/{/.}.mp3

