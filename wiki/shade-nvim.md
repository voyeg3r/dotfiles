```txt
file: shade-nvim.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

Dims your inactive windows, making it easier to see the active window at a glance.
