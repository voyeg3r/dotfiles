``` txt
file: tidy.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```



 =Usando o tidy dentro do vim=

  :!tidy -mi -css %

## Adding formatting to an xml document for easier reading

  tidy -i -xml <inputfile>

