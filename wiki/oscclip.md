```txt
Filename: oscclip.md
Last Change: Tue, 09 Aug 2022 12:27:25
tags: [ssh, clipboard, python, tools, termux]
```

## oscclip

oscclip is a little, zero dependency python utility which utilizes the system clipboard via OSC52 escape sequences. Using these sequences, the system clipboard is accessible via SSH as well. Terminal multiplexers, such as tmux and screen are supported.
Examples

Setting the clipboard

    $ echo "Foo" | osc-copy

Setting the clipboard and bypass terminal multiplexers

    $ echo "Foo" | osc-copy --bypass

Reading the clipboard

    $ osc-paste
    Foo
