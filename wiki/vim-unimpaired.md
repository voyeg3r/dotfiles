``` txt
file: vim-unimpaired.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```


# https://github.com/tpope/vim-unimpaired

pairs of handy bracket mappings

## Nice shortcuts

    [<space> ....... add new line before
    ]<space> ....... add new line after
    [e ............. exchange with preceding line
    ]e ............. exchange with next line
    ]a ............. next arg

tags: vim, nvim
