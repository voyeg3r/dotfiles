```txt
File: /home/sergio/.dotfiles/wiki/shift.md
Last Change:  Sun, 29 May 2022 - 11:34:00
tags: [shift, shell]
```

Shift is a builtin command in bash which after getting executed, shifts/move the command line arguments to one position left. The first argument is lost after using shift command. This command takes only one integer as an argument. This command is useful when you want to get rid of the command line arguments which are not needed after parsing them.

