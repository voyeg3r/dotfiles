``` txt
file: cancel.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

    lpq ................... lista trabalhos de impressão
    cancel -u joao ........ aborta impressões do usuário joao
    cancel -a ............. cancela todos os trabalhos de impressão

fonte: http://www.go2linux.org/how-to-clean-cancel-printer-queue-spool-spooler-linux
