``` txt
File: bitwarden.md
Author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
Date: dez 16, 2020 - 10:24
tags: [firefox, addons, tools, security]
```

#  bitwarden.md intro:
- https://bitwarden.com/help/article/cli/

Secure passowrd manager

## How to install?
- https://bitwarden.com/help/article/cli/

    bw --help

    bw login [email] [password]

    bw login --apikey

This will initiate a prompt for your personal client_id and client_secret. Once your session is authenticated using these values, you can use the unlock command

## biwtwarden cli show password:

    bw get password stackoverflow.com | pbcopy

    bw sync

## To open the side panel type:

    shift + alt + u

## Bitwarden cli
+ https://bitwarden.com/help/article/cli/#using-email-and-password

    doas npm install -g @bitwarden/cli

## save your bitwarde session to avoid typig your passowrd all the time:

```sh
# bitwarden login helper, it saves a session
function bwu(){
	export BW_SESSION=$(bw unlock --raw $1)
}
```

