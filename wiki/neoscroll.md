---
File: neoscroll.md
Author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
tags: [neovim, plugins]
---

#  neoscroll.md intro:

    use "karb94/neoscroll.nvim"

    use {
        "karb94/neoscroll.nvim",
        event = "WinScrolled",
        config = function()
            require('neoscroll').setup()
        end
    }

