``` txt
file: locale.md
author: Sergio Araujo
Last Change: Sat, 23 Jul 2022 14:12:24
```

## How to Set System Locale in Linux
+ https://www.tecmint.com/set-system-locales-in-linux/

    sudo update-locale LANG=LANG=pt_BR.UTF-8 LANGUAGE

or

    sudo localectl set-locale LANG=pt_BR.UTF-8
