``` txt
file: makepasswd.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

# Gerador de senhas do linux
mkpasswd - generate new password, optionally apply it to a user
