``` txt
file: touch.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
tags: [tools]
```

# The touch command
It is used to create files and change file timestamps

    touch file-{1..20}.txt

vim:ft=markdown
