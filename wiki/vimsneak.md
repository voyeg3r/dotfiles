``` txt
file: vim-sneak.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
tags: [vim, nvim, plugins]
```


The missing motion for Vim

## How to use it

    s{char}{char}

Type sab to move the cursor immediately to the next instance of the text "ab".

- If `s` is prefixed with [count] then |sneak-vertical-scope| is invoked,

## Install

    vim-plug

    Plug 'justinmk/vim-sneak'

## Get back to all [plugins](vimplugins.md)


