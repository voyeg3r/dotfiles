``` txt
file: cadaver.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

# cadaver http://webdav.4shared.com/

    cadaver - A command-line WebDAV client for Unix.

    digite seu usuário
    digite sua senha

    cd /diretorio
    mget *.ogv

No gnome shell pode-se acessar usando:

    davs://webdav.4shared.com

