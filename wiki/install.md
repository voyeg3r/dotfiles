``` txt
file: install.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

Criar um diretório, setar o dogno e o grupo em um só comando

    install -o user -g group -m 0700 -d /path/to/newdir

Copiando conteúdo e setando permissões a um só tempo:

    install -Dm755 /usr/share/doc/bspwm/examples/bspwmrc ~/.config/bspwm/bspwmrc
    install -Dm644 /usr/share/doc/bspwm/examples/sxhkdrc ~/.config/sxhkd/sxhkdrc

