```txt
File: /home/sergio/.dotfiles/wiki/picom.md
Last Change: Sat, 10 Sep 2022 11:09:04
tags: [desktop, compositor, tools, utils, screen, alpha, transparency]
```

## What is picom?
+ https://www.linuxfordevices.com/tutorials/linux/picom
+ https://man.archlinux.org/man/picom.1.en

Picom is a standalone compositor for Xorg, suitable for use with window managers that do not provide compositing. picom is a fork of compton, which is a fork of xcompmgr-dana, which in turn is a fork of xcompmgr.

## fork with some animations:
+ https://github.com/jonaburg/picom
+ https://github.com/ibhagwan/picom/tree/next-rebase (CURRENT)
+ https://www.reddit.com/r/voidlinux/comments/h9cx1j/picomibhagwan_available_as_a_template_for/

To be on the safe side I have created a fork ot the above repo: [here](https://github.com/voyeg3r/picom)

NOTE: You will need meson installed (super fast build system)

## A fork with build instructions for voidlinux:
+ https://github.com/ibhagwan/picom-ibhagwan-template

```sh
git clone --depth=1 https://github.com/void-linux/void-packages
cd void-packages
./xbps-src binary-bootstrap
echo XBPS_ALLOW_RESTRICTED=yes >> etc/conf

git clone https://github.com/ibhagwan/picom-ibhagwan-template
mv picom-ibhagwan-template ./srcpkgs/picom-ibhagwan

./xbps-src pkg picom-ibhagwan
sudo xbps-install --repository=hostdir/binpkgs picom-ibhagwan
```

Note #1: if you have xtools installed you can install the package by running xi -f picom-ibhagwan (instead of using xbps-install).

Note #2: before installing the package make sure to remove all other compton|picom packages with sudo xbps-remove picom compton.

## Fixing startup issues:
+ https://github.com/google/xsecurelock/issues/97#issuecomment-629232705
+ https://man.archlinux.org/man/picom.1.html
+ https://www.youtube.com/watch?v=HxbhkkfaVuo&t=479s

    # NOTE: doas xbps-install -Sy libva-glx{,-devel} mesa-intel-dri (if you have an intel gpu)
    picom --backend glx &

If you don't have nvidia gpu disable vsync:

    sed -i.backup '/vsync/s/true/false/' ~/.config/picom/picom.conf

## Differnt opacity for active and inactive terminals:

```sh
opacity-rule = [
    "90:class_g = 'URxvt' && focused",
    "60:class_g = 'URxvt' && !focused",
    "90:class_g = 'st' && focused",
    "60:class_g = 'st' && !focused",
    "90:class_g = 'st-256color' && focused",
    "60:class_g = 'st-256color' && !focused"
];
```
# vim:ft=markdown:et:sw=4:ts=4:cole=0:nospell:
