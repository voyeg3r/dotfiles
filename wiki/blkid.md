``` txt
file: blkid.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
tags: [uuid, tools, admin]
```

# Este comando tem que ser executado como root
Find UUID of your device

    blkid - command-line utility to locate/print block device attributes

    blkid
    /dev/sda1: UUID="8f2e84fe-fc1f-4147-b09b-d0e1d773a5db" TYPE="ext4"
    /dev/sda5: UUID="826eed8e-0a01-4649-9893-c76e9bd9f111" TYPE="swap

    sudo blkid /dev/sda1

Another way of doing that

``` bash
ls -l /dev/disk/by-uuid
total 0
lrwxrwxrwx 1 root root 10 ago 18 20:17 3639-3930 -> ../../sdb1
lrwxrwxrwx 1 root root 10 ago 18 18:28 4c7ad644-f0c0-4d7a-bea1-a9811a4a2241 -> ../../sda1
```

veja também [[vol_id]]
