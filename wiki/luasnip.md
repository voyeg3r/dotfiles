```txt
-- File: /home/sergio/.dotfiles/wiki/luasnip.md
-- Last Change:  Mon, 20 Jun 2022 - 18:52:35
```

## snippets engine written in pure lua:
+ https://cj.rs/blog/ultisnips-to-luasnip/
+ https://github.com/L3MON4D3/LuaSnip
+ https://raw.githubusercontent.com/sbulav/dotfiles/master/nvim/lua/config/snippets.lua
+ https://github.com/honza/vim-snippets.git


    s("trigger", {
    	t({"After expanding, the cursor is here ->"}), i(1),
    	t({"", "After jumping forward once, cursor is here ->"}), i(2),
    	t({"", "After jumping once more, the snippet is exited there ->"}), i(0),
    })

## local require snippet:

        s("lreq", fmt("local {} = require('{})", { i(1, "module_name"), rep(1)} ) ),

## Defining variables to use in isnippets:
+ https://sbulav.github.io/vim/neovim-setting-up-luasnip/

	local date = function() return {os.date('%Y-%m-%d')} end

	-- full date --> Wed, 09 Feb 2022 11:10
	s('fdate', f(bash, {}, 'date +"%a, %d %b %Y %H:%M"')),

	s({ -- see local date defiition
		trig = "date",
		namr = "Date",
		dscr = "Date in the form of Wed, 09 Feb 2022 11:37",
	}, {
		f(date, {}),
	}),

## Test if luasnip is loaded:

```lua
local status_ok, packer = pcall(require, "packer")
if not status_ok then
    return
end

if packer_plugins['LuaSnip'] and packer_plugins['LuaSnip'].loaded then
    print('LuaSnip is loaded')
end
```
