``` txt
file: mvconv.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

# convert filename charset

    convmv -r -f windows-1252 -t UTF-8 .
