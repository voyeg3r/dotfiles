``` txt
file: zenity.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```


# zenity.md

Zenity – Creates Graphical (GTK+) Dialog Boxes in Command-line and Shell Scripts

I have this alias

    alias pbcopy='xclip -selection clipboard'
    zenity --entry | pbcopy

    zenity --calendar
