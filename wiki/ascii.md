``` txt
file: ascii.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

# Art ascii e tabela ascii
É uma tabela que relaciona números binários/inteiros com
caracteres

## ascii art
são desenhos feitos com caracteres

    $ cowsay "Hi, How are you"

     _________________
    < Hi, How are you >
     -----------------
            \   ^__^
             \  (oo)\_______
                (__)\       )\/\
                    ||----w |
                    ||     ||
