``` txt
file: sponge.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```


* http://www.youtube.com/watch?v=ZeGzgVvBt3w&playnext_from=TL&videos=I2_3VXwmXiw

O comando sponge faz parte do pacote moreutils e não vem instalado por padrão

    column -t data.csv | sponge data.csv

