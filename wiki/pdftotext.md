``` txt
file: pdftotext.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```


## First of all you have to install
+ https://www.howtogeek.com/228531/

    sudo xbps-install -S poppler-utils

## converting pdf to text
+ https://askubuntu.com/a/307789/3798

    pdftotext -layout input.pdf output.txt
