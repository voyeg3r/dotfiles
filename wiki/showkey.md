``` txt
File: showkey.md
Author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
Date: fev 13, 2020 - 12:26
tags: [tags]
```


#  showkey.md intro:
showkey - examine the codes sent by the keyboard

    showkey -a
