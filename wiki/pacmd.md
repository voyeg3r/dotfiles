```txt
Filename: pacmd.md
Last Change: Wed, 07 Sep 2022 11:08:38
```

Reconfigure a PulseAudio sound server during
   runtime



## You can list the available sink names with

    pacmd list-sinks | grep name:
    name: <alsa_output.pci-0000_00_1b.0.analog-stereo>
