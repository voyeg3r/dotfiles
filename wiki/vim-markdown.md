``` txt
file: vim-markdown.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```
##  vim-markdown.md intro:
+ https://github.com/plasticboy/vim-markdown

O plugin vim-markdown tem um atalho `ge` para abrir links
ou criar arquivos novos entre outras funcionalidades legais

Esse plugin é citado [nesse](https://dev.to/konstantin/taking-notes-in-vim-revisited-558k) artigo do dev.to:

## Interesting Options

  ge ............. open link under the cursor (creat file if it does not exists)
  :Toc ........... open a quickfix index
  :InsertToc: Insert table of contents at the current line.

