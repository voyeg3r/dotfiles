``` txt
file: openssl.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

# How to compress and protect a folder in Linux using a password

Use the tar command to compress the whole folder named dir1 in the current
working directory:

    tar -cz dir1 | openssl enc -aes-256-cbc -e > dir1.tar.gz.enc

Hide it:

    mv -v dir1.tar.gz.enc .dir1.tar.gz.enc

Delete the original directory in Linux using the rm command:

    rm -rf dir1

To decrypt, run:

    openssl enc -aes-256-cbc -d -in dir1.tar.gz.enc | tar xz
