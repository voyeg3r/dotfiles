```txt
Filename: tokyonight.md
Last Change: Sun, 21 Aug 2022 11:40:43
```

## nice colorscheme written in pure lua

Changing the split window border:

    vim.g.tokyonight_colors = { border = "orange" }
