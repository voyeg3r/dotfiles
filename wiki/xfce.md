``` txt
File: xfce.md
Author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
tags: [desktop, linux]
```

##  xfce.md intro:
XFCE is a free and open source desktop environment

## exo-open

xfce4 tool to open preferred apps

## acessando via ssh usando o thunar (gerenciador de arquivos)
Pode digitar ssh que o próprio tunar coloca sftp na barra de endereços

``` sh
sftp://sergio@192.168.0.104/home/sergio/tmp/
```

## install new themes

Install ocs-url: An install helper program for items served
via OpenCollaborationServices ocs://, in case of voidlinux just type:

    sudo xbps-install -Sy ocs-url

+ [Papirus Grey](https://www.xfce-look.org/p/1576871)"
+ [sunrise](https://www.xfce-look.org/p/1258305)

## xfce no sound:

    doas xbps-install -Sy alsa-utils
    alsamixer

## change your dock
Plank is a elegant, simple and clean dock

    doas xbps-install -Sy plank

If you start 'plank' with `--preferences` it will show a panel where 
you can choose where and how it will appear on your desktop.

## New and beautiful star apps menu
+ https://www.pragmaticlinux.com/2021/03/install-and-configure-the-whisker-menu-as-your-xfce-start-menu/

    doas xbps-install xfce4-whiskermenu-plugin

    Right click (panel) > panel preferences > add a new item

## take screenshots with xfce4

    doas xbps-install -Sy xfce4-screenshooter

## Enabling transparency
+ https://askubuntu.com/a/100376/3798

Xfwm4-Composite-Editor is a tool that enables easy access to various settings
pertaining to the Xfce window manager in a graphical application with a simple
interface.

Open: 

    xfwm4-tweaks-settings > compositor > enable screen compositor

