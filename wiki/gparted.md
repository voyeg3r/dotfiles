``` txt
file: gparted.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39

# Introdução
Particionador gráfico, site oficial: http://gparted.sourceforge.net/

## Veja também
* Clonezilla: http://clonezilla.org/download/sourceforge/
