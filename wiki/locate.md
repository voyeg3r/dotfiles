``` txt
file: locate.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

# localizar arquivos mp4

    locate .mp4

## como atualizar o banco de dados do locate?

    locatedb

No voidlinux

    sudo xbps-install -S mlocate
    sudo xbps-alternatives -s mlocate -g locate
