```txt
File: vimplugins.md
Author: Sergio Araujo
Last Change: Sun, 21 Aug 2022 11:39:32
tags: [nvim, vim, plugins]
```

## Intro

This file describes some useful vim plugins

+ [emmet vim](emmet-vim.md) working with html files
+ [vim-unimpaired](vim-unimpaired.md) pairs of handy bracket mappings
+ [comment.nvim ](comment-nvim.md) comment stuff out (lua plugin)
+ [fzf.md](fzf.md) fuzzy finder for files, commands and a lot more
+ [vim sneak](vimsneak.md) Sneak - the missing motion for Vim
+ [Vim Markdown](vim-markdown.md) Um plugin para facilitar a edição de arquivos markdown
+ [vim fugitive](https://github.com/tpope/vim-fugitive) git melhorado para o vim
+ [diffchar.vim](https://github.com/rickhowe/diffchar.vim) a better diff on vim
+ [vim-smoothie](vim-smoothie.md) Nice scrolling
+ [nvim-tree](nvim-tree.md) nerd tree like plugin (but in lua)
+ [buftabline](buftabline.md) show your opened buffers
+ [prettier](prettier.md)
+ [vim-autoswap](https://github.com/gioele/vim-autoswap)
+ [vim-speedating](tpope/vim-speeddating)
+ [nvim-beacom](nvim-beacom.md) DanilaMihailov/beacon.nvim (flashes the cursorline)

## Nvim plugins:

+ [snippy][snippy.md] snippets engine (have a look)
+ [packer](packer.md) Plugin management
+ [pounce.nvim](pounce-nvim.md)
+ [impatient](lewis6991/impatient.nvim) lazy load plugins
+ [surround.nvim](surround-nvim.md)
+ [nvim-surround](nvim-surround.md)
+ [comment.nvim](comment-nvim.md)
+ [neoscroll](neoscroll.md) Nice scrolling (lua based)
+ [shade-nvim](shade-nvim.md) dims your inactive windows, making it easier to see the active window at a glance.
+ [vim-diminactive](vim-diminactive) vim alternative to shade-nvim
+ [nvim-markdown](nvim-markdown.md) Fork of vim-markdown with extra functionality.
+ [gitsigns-nvim](gitsigns-nvim.md) Signs for added, removed, and changed lines
+ [which-key](which-key.md) shows and runs your mappings
+ [lspsaga](lspsaga.md) better LSP options
+ [nvim-cursorword](nvim-cursorword) hightlights word under the cursor
+ [edluffy/specs.nvim](https://github.com/edluffy/specs.nvim) better than beacon becaus it is written in pure lua
+ [luasnip](luasnip.md)
+ [matchparen-nvim](matchparen-nvim.md) - faster than matchparen-vim because it uses treesitter
+ [mason.nvim](mason-nvim.md)
+ [lsp-zero](https://github.com/VonHeikemen/lsp-zero.nvim) - easy lsp config
+ [shade.nvim](https://github.com/sunjon/Shade.nvim) dim inactive windows
+ [snippet-converter.nvim](https://github.com/smjonas/snippet-converter.nvim)
+ [mkdnflow.nvim](https://github.com/jakewvincent/mkdnflow.nvim)
+ [neopm](https://github.com/ii14/neopm) new package manager for test
+ [readline.nvim](https://github.com/linty-org/readline.nvim) Use readline shortcuts easily
+ [substitute.nvim](https://github.com/gbprod/substitute.nvim) similar to vim-exchange but written in lua (sxiw)
+ [lsp-zero](https://github.com/VonHeikemen/lsp-zero.nvim) Easier LSP config

## nvim colorschemes:
[tokyonight](tokyonight.md)

<!- vim: set nospell  --->
