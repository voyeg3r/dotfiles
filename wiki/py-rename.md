``` txt
File: py-rename.md
Author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
Date: mar 03, 2021 - 18:15
tags: [python, tools, rename]
```

#  py-rename.md intro:
+ https://pypi.org/project/py-rename/

type:

    py-rename -h


    py-rename match ".+\(00(\d{2})\).+"

    ('matched ab12+Red+(0000).txt',)
    ('matched ab12+Red+(0001).txt',)
    ('matched ab12+Red+(0002).txt',)
    ('files matched: 3',)

    py-rename -n rename ".+\(00(\d{2})\).+" "\1-Red.txt"

