``` txt
file: sync.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```


Sincroniza discos, é usado geralmente antes de desmontar
um disco para segurança de dados.

também é usado após o uso do comando [[ComandoDd|dd]]
