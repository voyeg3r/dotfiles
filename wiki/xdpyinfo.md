``` txt
file: xdpyinfo.md
author: Sergio Araujo
Last Change: Wed, 07 Sep 2022 10:07:10
tags: [display, x11, tools]
```

## display information utility for X

If you want to get your $DISPLAY properly try this:

    awk '/dimensions/ {print $2}' <(xdpyinfo)

## Find the DPI value for your screen
+ https://winaero.com/find-change-screen-dpi-linux/

To find the current DPI value of the screen used by Xserver, do the following.

    Open your favorite terminal emulator.
    Type or copy-paste the following command:

    xdpyinfo | grep -B 2 resolution

