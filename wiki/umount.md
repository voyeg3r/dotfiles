``` txt
file: umount.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```



## Desmonta dispositivo ocupado


umount -l /media/smb

## pode também usar fuser

fuser -mkv /media/dispositivo


