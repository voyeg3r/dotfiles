```txt
File: ttyrec.md
Last Change:  Sun, 29 May 2022 - 08:58:38
tags: [tty, recording, terminal, tools, utils]
```

## Install on voidlinux:

    sudo xbps-install -Sy ttrec

Also install ttygif: https://github.com/icholy/ttygif

## How to use it:
+ https://www.thegeekdiary.com/ttyrec-record-terminal-session-in-linux/

    ttyrec

    ttyplay ttyrecord
