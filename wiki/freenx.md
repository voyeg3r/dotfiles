``` txt
file: freenx.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

# What is FreeNX
FreeNX is a system that allows you to access your desktop from another machine over the Internet. You can use this to login graphically to your desktop from a remote location.
