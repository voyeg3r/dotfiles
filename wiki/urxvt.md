``` txt
File: urxvt.md
Author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
tags: [terminals, tools]
```

# rxvt-unicode

## how to copy & paste on urxvt?
+ https://unix.stackexchange.com/questions/212360

    copy              =    Ctrl - Alt - c
    paste             =    Ctrl - Alt - v 
    primary selection =    shift - insert
