File: /home/sergio/.dotfiles/wiki/ytfzf.md
Last Change: Sat, 17 Sep 2022 11:14:47

## site:
+ https://github.com/pystardust/ytfzf
+ https://www.youtube.com/channel/UC3yaWWA9FF9O

A POSIX script that helps you find Youtube videos (without API) and
opens/downloads them using mpv/youtube-dl
