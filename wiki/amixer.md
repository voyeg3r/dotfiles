``` txt
file: amixer.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

# amixer is a command-line mixer for ALSA soundcard driver.

Increase volume by 10%

    amixer set 'Master' 10%+

## Decrease volume by 10%

    amixer set 'Master' 10%-

## Set volume to 10%

    amixer set 'Master' 10%

## Set volume to 80%

    amixer set 'Master' 80%

## Shows a complete list of simple mixer controls

    amixer scontrols

amixer is a command-line mixer for ALSA soundcard driver.

Set volume to 80%

    amixer set 'Master' 80%

Shows a complete list of simple mixer controls

    amixer scontrols
