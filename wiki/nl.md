``` txt
file: nl.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

# Introdução

Show file with line numbers

    nl /etc/passwd | less

