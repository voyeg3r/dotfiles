``` txt
file: readlink.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```
#  readlink.md intro:


readlink - print resolved symbolic links or canonical file names

example:

    readlink ~/.config/nvim
