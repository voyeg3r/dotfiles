```txt
Filename: mason-nvim.md
Last Change: Fri, 19 Aug 2022 19:00:13
```
Reference:
+ https://github.com/Blovio/NeoVim_Essentials

    -- mason --> mason-lspconfig --> lspconfig
    -- must be setup in this order
    require "plugins/configs/mason"
    require "plugins/configs/mason-lspconfig"
    require "plugins/configs/lspconfig"
