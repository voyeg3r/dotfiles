```txt
File: dicasnvim.md
 vim:ft=markdown et sw=4 ts=4:
Last Change: Fri, 16 Sep 2022 13:03:05
tags: [vim, nvim]
```

## Dicasnvim.md useful links:

+ https://www.gatlin.io/blog/post/lua-primer-for-neovim
+ https://github.com/mjlbach/defaults.nvim
+ https://jdhao.github.io/2021/07/11/from_vim_plug_to_packer/
+ https://elianiva.my.id/post/improving-nvim-startup-time
+ https://github.com/topics/neovim-colorscheme?l=lua
+ https://github.com/shaunsingh/vimrc-dotfiles/tree/main/.config/nvim
+ https://github.com/kevinhwang91/nvim-bqf (better qf requirement â†’ nightly)
+ https://github.com/TimUntersberger/neogit (amazing git tool)
+ https://github.com/ayamir/nvimdots (dotfiles to study)
+ https://dev.to/creativenull/installing-neovim-nightly-alongside-stable-10d0
+ https://github.com/NTBBloodbath/cheovim
+ https://github.com/NTBBloodbath/doom-nvim
+ https://benfrain.com/refactor-your-neovim-init-lua-single-file-to-modules-with-packer/
- https://github.com/Leiyi548/nvim
+ https://vim.help/table-of-contents
+ https://github.com/rockerBOO/awesome-neovim (plugins alternative and more)
+ https://benfrain.com/the-best-neovim-color-schemes-in-2021/
+ https://terminalroot.com.br/2021/11/lsp-autocomplete-e-machine-learning-neovim-com-lua.html
+ https://github.com/ayesumit/Nvimfy
+ https://github.com/NvChad/NvChad
+ https://github.com/serhez/nvim-conf (a new config for testing)
+ https://github.com/kabinspace/AstroVim

## How viminfo (shada) works?

    set viminfo=%,<800,'10,/50,:100,h,f0,n~/.vim/cache/.viminfo
    "           | |    |   |   |    | |  + viminfo file path
    "           | |    |   |   |    | + file marks 0-9,A-Z 0=NOT stored
    "           | |    |   |   |    + disable 'hlsearch' loading viminfo
    "           | |    |   |   + command-line history saved
    "           | |    |   + search history saved
    "           | |    + files marks saved
    "           | + lines saved each register (old name for <, vi6.2)
    "           + save/restore buffer list

## Jump throught markdown headings:
+ https://www.reddit.com/r/vim/comments/p7xcpo/comment/h9obuvl/

    :vimgrep /^#/ %
    :copen

    -- quickfix mappings
    map('n', '[q', ':cprevious<CR>')
    map('n', ']q', ':cnext<CR>')
    map('n', ']Q', ':clast<CR>')
    map('n', '[Q', ':cfirst<CR>')

## conceal markdown links:
+ https://www.reddit.com/r/neovim/comments/rw4imi/comment/hrdacsm/

## Compile nvim from the source code:
+ https://colleowino.github.io/building-neovim
+ https://github.com/neovim/neovim/wiki/Building-Neovim

```sh
git clone https://github.com/neovim/neovim
git checkout stable
make
make test
sudo make install
```

## scroll both windows at once:
+ https://stackoverflow.com/a/18466534/2571881

    :windo set scrollbind

## nvim as manpage:

    function man () {
    /bin/sh -c "unset PAGER"
    nvim -c 'set ft=man bt=nofile bh=hide noswapfile' -c "Man $1" -c 'silent! only' -c 'nmap q :bd!<CR>'
    }

    /bin/sh -c "unset PAGER;col -b -x | vim -R -c 'set ft=man nomod nolist' -c 'map q :q<CR>' -c 'map <SPACE> <C-D>' -c 'map b <C-U>' -c 'nmap K :Man <C-R>=expand(\"<cword>\")<CR><CR>' -"

## My current configuration comes from this repo:
+ https://github.com/LunarVim/Neovim-from-scratch

    git clone https://github.com/LunarVim/Neovim-from-scratch.git

## Secure modelines

```lua
-- https://github.com/numirias/security/blob/master/doc/2019-06-04_ace-vim-neovim.md#patches
-- https://stackoverflow.com/a/41994024/2571881
if vim.fn.has('patch-8.1.1366') then
    vim.opt.modelines=5
    vim.opt.modelineexpr = false
    vim.opt.modeline = true
else
    vim.opt.nomodeline = true
end
```

Modeline issues:

    # vim: set cursorline
		^^^ issue: use of the word set without a trailing colon (:)

    # vim:cursorline
		^ issue: no space after the colon

    #vim: cursorline
	^ issue: no space before the word vim

## Get lua tables values:

	-- in case of tables (lists)
	print(vim.inspect(vim.opt.listchars:get()))
	--  { space = "_", tab = ">~", }

	lua print(vim.opt.autoindent:get())

## Question: How to assign "printed" value to a variable?
+ https://www.reddit.com/r/neovim/comments/t2gu18

    local result = vim.fn.execute[[highlight StatusLine]]

## Check startup time:
+ https://github.com/rhysd/vim-startuptime

    go install github.com/rhysd/vim-startuptime@latest
    vim-startuptime

	-- IMPROVE NEOVIM STARTUP
	-- https://github.com/editorconfig/editorconfig-vim/issues/50
	vim.g.loaded_python_provier=1
	vim.g.python_host_skip_check = 1
	vim.g.python_host_prog='/bin/python2'
	vim.g.python3_host_skip_check = 1
	vim.g.python3_host_prog='/bin/python3'
	vim.opt.pyxversion=3
	-- if vim.fn.executable("editorconfig") then
	-- 	vim.g.EditorConfig_exec_path = '/bin/editorconfig'
	-- end
	vim.g.EditorConfig_core_mode = 'external'

## Defer some plugins on startup:
+ https://vonheikemen.github.io/devlog/tools/the-illusion-of-fast-startup-times-in-vim/

Loggins startup:

    vim --startuptime vim.log

## clear caches
Instead of restarting the system this will clear all caches

    echo 1 > /proc/sys/vm/drop_caches

## Trying another nvim "distro"

    git clone https://github.com/brainfucksec/neovim-lua.git
    cd neovim-lua/
    cp -Rv nvim ~/.config/

## Controling formatoptions - avoid continue commenting with o or Enter:
+ https://superuser.com/a/271024/45032
+ https://stackoverflow.com/a/16032415/2571881

You can use the 'formatoptions' option  to influence how Vim formats text.
'formatoptions' is a string that can contain any of the letters below.  The
default setting is "tcq".  You can separate the option letters with commas for
readability. **See fo-talbe**

letter  meaning when present in 'formatoptions'

    t  Auto-wrap text using textwidth
    c  Auto-wrap comments using textwidth, inserting the current comment
       leader automatically.
    r  Automatically insert the current comment leader after hitting
       <Enter> in Insert mode.
    o  Automatically insert the current comment leader after hitting 'o' or
       'O' in Normal mode.

    au FileType * set fo-=c fo-=r fo-=o

You could check where the option is set issuing with following commands:

    :5verbose set fo?
    :5verbose setl fo?

## Setting up lsp servers (language servers)

Use the [lsp](lsp.md)

## Sort from 4th column until the end:

In this case I already have the selection `'<,'>`
    :'<,'>sort /\%4v/

## Align by a given char:
Align by equal sign

+ https://stackoverflow.com/a/51462785/2571881

    :% ! column -t -s= -o=

## Color picker using telescope (lua)
+ https://pastebin.com/3qi2THX8

## How to scroll in another window without switching to it?
+ https://vi.stackexchange.com/a/26604/7339

winnr('j') returns Window number of a window below the current one. win_getid() translates Window number to :h winid. Then win_execute() runs an arbitrary command as if that window were active (but display is not updated, and auto-commands are not triggered).

Note: win_execute() is not available in Neovim.

	call win_execute(win_getid(winnr('j')), "normal! \<C-d>")

## Getting the file name:

    short file name just the "tail"
    expand("%:t")

    1 Ctrl-g

To insert the full path filename just press (in insert mode):

    Ctrl-r = expand("%:p")
    or create an alias like this

    using insert abbreviation in lua
    vim.cmd([[inoreab Fname <c-r>=expand("%:p")<cr>]])

## Getting home dir:

    :lua print(vim.loop.os_homedir())

## Lua map util

```lua
-- -- https://www.notonlycode.org/neovim-lua-config/
-- shortcut -> lhs     command -> rhs
-- lhs -> left hand side  rhs -> right rand side
function map(mode, shortcut, command, opts)
  local options = { noremap = true }
  if opts then
    options = vim.tbl_extend("force", options, opts)
  end
  vim.api.nvim_set_keymap(mode, shortcut, command, options)
end

nmap("<F10>", ':echom "just a test"<cr>')

nmap("<F11>", "<cmd>lua require('utils').flash_cursorline()<cr>", { silent = true })
```

## Restore cursor position when yanking in visual mode:

    -- Make visual yanks place the cursor back where started
    map("v", "y", "ygv<Esc>")

## Better gx mapping

    map('', 'gx', '<Cmd>call jobstart(["xdg-open", expand("<cfile>")], {"detach": v:true})<CR>', {})

## Swiching buffers:

Along with alternate file you can do 5<Ctrl-6> to jump to the fifth buffer.

We can also open a given buffer in an alternative window, something like:

    Ctrl-w 3 Ctrl-6

I have also a mapping '<leader>b' for Telescope buffers and

	<A-,>  ................. BufferPrevious
	<A-.>  ................. BufferNext

## Compile sumneko lua language server for neovim:
+ https://github.com/xiyaowong/coc-sumneko-lua/issues/6

## Install tree-siter:
+ https://github.com/nvim-treesitter/nvim-treesitter/issues/1097

    sudo npm install --location=global tree-sitter

## Better gf

At the moment neovim has a pretty good gf behavior because it uses lsp or treesitter to solve the file path.

    "A native gf fail if the file does not exist
    nnoremap gf :edit <cfile><cr>
    map('n', 'gf', ':split <cfile><cr>')

    gF ............. follow the file (and jumps to the line number)

    gF 	open in the same window
    <c-w>F 	open in a new window (Ctrl-w F)

Open files when the extension does not appear on the current file, like lua modules:

    require('core.utils')

    :set suffixesadd+=.js

## Jump to the next curly braces }

    ]}
    ]]

## Edit a new file in a vertical split window:

    :vsp /pat/to/file

    Or using telescope press Ctrl + v to vertical split or Ctrl + x to horizontal split

## Add filename at lualine center (plugin):

```lua
-- source: https://benfrain.com/neovim-tips-and-tricks-volume-one/
lualine_c = {
      {"diagnostics", sources = {"nvim_lsp"}},
      function()
        return "%="
      end,
      "filename"
    },
```

## Compilar sumneko lua para sua arquitetura

```sh
git clone git@github.com:sumneko/lua-language-server.git #follow build instructions
git submodule update --init --recursive
$ cd 3rd/luamake && ./compile/install.sh && cd ../..  && ./3rd/luamake/luamake rebuild && cp bin/Linux/lua-language-server ~/.config/coc/extensions/coc-sumneko-lua-data/sumneko-lua-ls/extension/server/bin/Linux/
```

Sumneko root path:

```lua
-- Coloque no arquivo lspconfig.lua
local sumneko_root_path = vim.fn.stdpath('data') .. "/lsp_servers/sumneko_lua/extension/server"
local sumneko_binary = sumneko_root_path.."/bin/Linux/lua-language-server"
```

## Copy pattern to a register (copiar padrÃ£o para um registro)

    :g/https:/yank A

## Tab completion in the command line

    set wildcharm=<C-z>
    cnoremap <expr> <Tab>   getcmdtype() =~ '[\/?]' ? "<C-f>A<C-n>" : "<C-z>"
    cnoremap / /<tab>

## Better navigation with harpoon plugin:
+ https://github.com/ThePrimeagen/harpoon

  use 'nvim-lua/plenary.nvim' " don't forget to add this one if you don't have it yet! use 'nvim-lua/popup.nvim' use
  'ThePrimeagen/harpoon'

To mark a file:

    :lua require("harpoon.mark").add_file()

In my case I have a mpping to open the harpoon menu:

    map('n','<leader>h', '<cmd>:lua require("harpoon.ui").toggle_quick_menu()<cr>')

## A working example for nvim-cmp, luasnip and friendly-snippets

+ https://github.com/mxdevmanuel/dotfiles
+ https://github.com/L3MON4D3/LuaSnip/issues/201
+ https://www.reddit.com/r/neovim/comments/ri2u3m/luasnip_now_has_a_snipmatestyle_loader/
+ https://github.com/L3MON4D3/LuaSnip/blob/master/DOC.md#snipmate-snippets-loader
+ https://sbulav.github.io/vim/neovim-setting-up-luasnip/

See from_vscode for an explanation of opts. If opts.paths is ommited, snippets are loaded from any directory named
snippets located in the runtimepath.

    require("luasnip/loaders/from_vscode").load({ paths = { "~/.config/nvim/custom_snippets" } })

As the snipmate snippet format is fundamentally the same as vscode, it can also be loaded.

    require("luasnip.loaders.from_snipmate").load(opts) -- opts can be ommited

Lazy loading is also available with the snipmate-loader.

    require("luasnip.loaders.from_snipmate").lazy_load(opts) -- opts can be ommited

Snipmate snippets can be loaded like this:

	require("luasnip.loaders.from_snipmate").load({
	paths = {
		vim.fn.stdpath('config') .. '/snippets',
	}
	})

In your luasnip config file:

	local ls = require("luasnip")
	ls.filetype_extend("all", { "_" })

## Insert a snippet via function:
TODO: test if it is already dinamic

    :execute "normal i" . "_skel" . "\<Tab>"

## Disable red underscores in markdown:
+ https://github.com/tpope/vim-markdown/issues/21
+ https://github.com/tpope/vim-markdown/issues/21#issuecomment-986248462

    syn match markdownError "\w\@<=\w\@="

    Add hi link markdownError NONE to after/syntax/markdown.vim

## Put 'messages' into clipboard:
+ https://vi.stackexchange.com/questions/31067

    :let @+=execute('messages')

## Set commentstring

    vim.bo.commentstring = '//%s'

For languages that have comment symbols at the begining and at the end we can do:

    vim.bo.commentstring = '<!--%s-->'

## Rename files using vim:

Let's say you have these files

    file-1.txt
    file-2.txt
    file-3.txt

You create the command (mv old new) in this case adding leading zeros to the numbers

    :%s/\v(file-)(\d+)(\.txt)/\='mv '. submatch(0) . ' ' . submatch(1) . printf('%03d', submatch(2)) . submatch(3)

Then you call the shell:

    :sil! w !bash

Bulk rename -> https://vim.fandom.com/wiki/Bulk_rename_files_with_Vim

    \ls | vim -

For example, to rename the files to lowercase:

    :%s/.*/mv -i '&' \L'&'/g
    :sil! w !sh

An alternative method of executing the commands is:

    :%!bash

## Subtracting seconds from time:

    time test 00:02:01
    time test 00:01:01
    time test 00:04:59

    :1,3s/00.*\ze$/\=system('date -u -d "'.submatch(0).' +0000 -02 sec" +"%H:%M:%S"')/g
    :1,3s/00.*\ze$/\=system('date -u -d "'.submatch(0).' +0000 -02 sec" +"%H:%M:%S"| tr "\n" " "')/g

## Adding numbers with leading zeros:
Let's say you have a list like this:

    Gonzaguinha_-_Lindo_Lago_do_Amor.mp3
    Gonzaguinha_-_Comecaria_tudo_outra_vez.mp3
    Gonzaguinha_de_Volta_ao_Comeco.mp3
    Gentileza_-_Gonzaguinha_Ao_Vivo.mp3
    Diga_la_coracao.mp3
    Anunciacao.mp3
    E_Vamos_A_Luta.mp3
    Tiro_Ao_Alvaro.mp3
    Flor_de_Lis.mp3
    Aguas_De_Marco.mp3

And you want to add at the beginning of each line 01, 02, 03 etc:

    let c=0 | g/^/ let c+=1 | s//\=printf('%02d', c) . '-'

## Make clipboard register blockwise so that you can paste as a column:

    :call setreg("+", getreg("+"), "b")

Also removing '^j' "Enter" from the clipboard register:
the ^j means enter or newline

    :let @+=substitute(@+,'\n', '', 'g')

## Merge two files:
+ https://stackoverflow.com/q/72237807/2571881

Open the two files:

    vim -o file1.txt file2.txt

    function! Join_files()
        wincmd j
        %y
        wincmd k
        call setreg('0', getreg('0'), 'b')
        normal gg
        .s/$/     /
        normal $"0p
        %s/\s\+/ /g
    endfunction

    :@0
    :call Join_files()


## Replace mode

    like insert in word text processor, just type R in normal mode

## Repository to study

    git clone https://github.com/voyeg3r/vimrc-dotfiles.git

## Lsp language servers:

    c .......... ccls  -> doas xbps-install -Sy ccls

## Refatorando cÃ³digos no neovim
+ https://stackoverflow.com/a/69482236/2571881

  :vimgrep '\v( |,)\zstest' \*_/_.c :cfdo %s/\v( |,)\zstest/\U& :cfdo update

## Creating desktop icon for neovim

```sh
    cat <<EOF > ~/.local/share/applications/nvim.desktop
    [Desktop Entry]
    Name=Neovim
    GenericName=Text Editor
    GenericName[pt]=Text Editor
    Comment=Edit text files
    TryExec=nvim
    Exec=nvim %F
    Terminal=true
    Type=Application
    Encoding=UTF-8
    Icon=nvim
    Categories=Utility;TextEditor;
    StartupNotify=false
    Keywords=Text;editor;
    MimeType=text/english;text/plain;text/x-makefile;text/x-c++hdr;text/x-c++src;text/x-chdr;text/x-csrc;text/x-java;text/x-moc;text/x-pascal;text/x-tcl;text/x-tex;application/x-shellscript;text/x-c;text/x-c++;
    EOF
```

## Getting cursor positon in lua

```lua
    local linha = vim.api.nvim_win_get_cursor(0)[1]

-- roll = linha   coll = coluna
local r,c = unpack(vim.api.nvim_win_get_cursor(0))
print(r, c)

lua vim.api.nvim_win_set_cursor({0},{15,0})
--onde zero Ã© a janela atual, 15 Ã© o nÃºmero da linha e 0 Ã© a coluna
```

## The jumplist issue

For jumping in the current buffer (only) prefer using `g;` and `g,`

    g;
    g,

I also have a mapping 'gl' --> '`.' mnemonic 'go last (change)'

## Running macros faster
+Â https://vi.stackexchange.com/a/31888/7339

Avoid accessing system clipboard in the macro.

Accessing external system clipboard +, * introduces additional cost, when compared with accessing internal registers. It may even freeze the macro replaying forever (run macro on 6000 lines in my test).

```markdown
:noautocmd :norm @q to disable events temporarily during macro running. Or
:set eventignore=all before macro running, and set it back after.

:sil! argdo :noautocmd norm! @a
:sil! argdo :noa norm! @a
```

## Test a new set up
+ https://www.reddit.com/r/neovim/comments/p9z2ii
- https://github.com/NTBBloodbath/cheovim

```markdown
alias simple-nvim=XDG_CONFIG_HOME=~/.simple-config nvim

Now you can use nvim to get your regular configuration and simple-nvim to use the other configuration.
```

## Nvim-cmp settings
+ https://gist.github.com/mengwangk/324c3aed377b94bf6d0da07f53205a7a

## Start insert at the right indenting

Just press 'S'

## Getting hlsearch status

    ``` lua
    local hls_status = vim.v.hlsearch
    ```

Specific neovim tips

```python
pip3 install --user pynvim
pip2 install --user pynvim
pip3 install --user jedi
```

## Framework for nvim
+ https://www.lunarvim.org/

## Reselect last pasted text
+ https://stackoverflow.com/a/4317090/2571881

    nnoremap <expr> g<c-v> '`[' . getregtype()[0] . '`]'

## Preserve function in lua
+ https://bit.ly/3g6vYIW

```lua
-- https://bit.ly/3g6vYIW
function _G.preserve(cmd)
    cmd = string.format('keepjumps keeppatterns execute %q', cmd)
    local original_cursor = vim.fn.winsaveview()
    vim.api.nvim_command(cmd)
    vim.fn.winrestview(original_cursor)
end
preserve('%s/\\s\\+$//e')
```

## Get current buffer number:

    :lua print(vim.api.nvim_get_current_buf())

## Run current line:
+ https://www.reddit.com/r/vim/comments/s26aau/comment/hscl6n1/

    :Ctrl-r Ctrl-l

## Squeeze blank lines

```lua
M = {}

M.squeeze_blank_lines = function()
    -- references: https://vi.stackexchange.com/posts/26304/revisions
    if (vim.bo.filetype ~= 'binary') or (vim.bo.filetype ~= 'diff') then
        local old_query = vim.fn.getreg('/')    -- save search register
        M.preserve('sil! 1,.s/^\\n\\{2,}/\\r/gn') -- set current search count number
        local result = vim.fn.searchcount({maxcount = 1000, timeout = 500}).current
        local line, col = unpack(vim.api.nvim_win_get_cursor(0))
        M.preserve('sil! keepp keepj %s/^\\n\\{2,}/\\r/ge')
        M.preserve('sil! keepp keepj %s/\\v($\\n\\s*)+%$/\\r/e')
        if result > 0 then
            vim.api.nvim_win_set_cursor({0}, {(line - result), col})
        end
        vim.fn.setreg('/', old_query)           -- restore search register
    end
end

return M
```

## Converter datas

```vim
" This function requires you select the numbers
" source: https://vi.stackexchange.com/a/4699/7339
" o formato da data tem que estar ano mÃªs dia
fun! ConvDate()
    try
        let l:a_save = @a
        norm! gv"ay
        let @a = '"' . @a . '"'
        " let var=system('~/.dotfiles/algorithm/shell/bash/convdate ' . @a)
        let timestamp=system('date +"%s" -d ' . @a)
        let new_date =system('LC_ALL=en_US;' . 'date +"%a, %b %d %Y" -d '. "@" . timestamp)
        let @a = new_date
        ec "Your new date is: " . new_date
        norm! gv"ap
        norm! kJ
    finally
        let @a = l:a_save
    endtry
endfun

" Execute this file
" https://sbulav.github.io/vim/neovim-telescope-github/
function! s:save_and_exec() abort
  if &filetype == 'vim'
    :silent! write
    :source %
  elseif &filetype == 'lua'
    :silent! write
    :lua require("plenary.reload").reload_module'%'
    :luafile %
  endif

  return
endfunction
" save and resource current file
nnoremap <leader><leader>x :call <SID>save_and_exec()<CR>
```

## Current ultisnips on my nvim

(as currently settled in Aug 10 2021)

    pressing tab will show the snippet content
    and an Enter will insert it. The <C-j> trigger also works

## Telescope nvim needs ripgrep

    doas xbps-install -Sy ripgrep

## How to get line number using lua?

- http://neovim.io/doc/user/api.html#nvim_win_get_cursor()

```lua
lua print(vim.api.nvim_win_get_cursor(0)[1])

api = vim.api
function line()
    local cline = api.nvim_win_get_cursor(0)[1]
    return cline
end
```

## Avoid loosing your text adding undo points in the line:
+ https://www.youtube.com/watch?v=hSHATqh8svM

```
map('i', '.', '.<C-g>u')
map('i', ',', ',<C-g>u')
map('i', '?', '?<C-g>u')
map('i', '!', '!<C-g>u')
map('i', ':', ':<C-g>u')
map('i', ';', ';<C-g>u')
```

## Get filetype with lua
+ https://vi.stackexchange.com/questions/27897/

filetype is a buffer-local option, so you can access it with:

    :lua print(vim.bo.filetype)

Get option:
+ https://vonheikemen.github.io/devlog/tools/configuring-neovim-using-lua/

  :lua print(vim.opt.lazyredraw:get())

## Disabling some builtin plugins:

```lua
-- disable builtins plugins
local disabled_built_ins = {
    "netrw",
    "netrwPlugin",
    "netrwSettings",
    "netrwFileHandlers",
    "gzip",
    "zip",
    "zipPlugin",
    "tar",
    "tarPlugin",
    "getscript",
    "getscriptPlugin",
    "vimball",
    "vimballPlugin",
    "2html_plugin",
    "logipat",
    "rrhelper",
    "spellfile_plugin",
    "matchit"
}

for _, plugin in pairs(disabled_built_ins) do
    g["loaded_" .. plugin] = 1
end

```
