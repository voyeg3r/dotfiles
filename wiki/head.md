``` txt
file: head.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

O comando head por padrão exibe as 10 primeiras linhas de um arquivo
caso queira exibir apenas as 5 primeiras linhas faça:

head -5 /etc/passwd

