``` txt
file: fzy.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

# fzy.md
+ https://github.com/jhawthorn/fzy

fzy is a fat, simple fuzzy text selector for terminal with advanced
scorring algorithm.

   ls -l $(find -type f | fzy)

   nvim $(ag --hidden . -l -g '' | fzy)
