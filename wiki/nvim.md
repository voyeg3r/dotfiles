``` txt
file: nvim.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
abstrac: Nvim tips
tags: [vim, nvim, neovim]
```
#  nvim.md intro:
hyperextensible Vim-based text editor

## sites to learn Vim

1 - https://www.vim.so/

## archlinux install
To install neovim with full python3 and python2 support run:

    sudo pacman -S xclip
    sudo pacman -S python-pip
    sudo pacman -S python2-pip
    sudo pip3 install neovim
    sudo pip3 install neovim-remote

    It is recommended to use the --user flag when installing the neovim package:

## lookbehind and lookahead regexes

``` viml
            lookbehind 	lookahead
positive 	(atom)\@<= 	(atom)\@=
negative 	(atom)\@<! 	(atom)\@!

In very magic mode:
            lookbehind 	lookahead
positive 	(atom)@<= 	(atom)@=
negative 	(atom)@<! 	(atom)@!
```

## jumping faster

``` viml
]] Jump forward to begin of next toplevel
[[ Jump backwards to begin of current toplevel (if already there, previous toplevel)
]m Jump forward to begin of next method/scope
[m Jump backwords to begin of previous method/scope

][ Jump forward to end of current toplevel
[] Jump backward to end of previous of toplevel
]M Jump forward to end of current method/scope
[M Jump backward to end of previous method/scope
```

## Insert semicolon and go to the next line or go to the next line
+ https://stackoverflow.com/questions/66397197

    nmap <silent> <expr> <Leader>c strpart(getline('.'), col('$')-2, 1) == ";" ? "j" : "A;<Esc><Home>j"

## User install
The Python package neovim was renamed to pynvim.

    sudo pip2 install pynvim
    sudo pip3 install pynvim

    The --user flag installs neovim in a directory within your home directory.
    The limitation with this is that every user account on the system will need
    to perform this step. You can confirm the location and that your
    environment permits user packages by running: python -m site

## ToggleMovement

``` viml
" Make some command toggle for more speed!
" https://ddrscott.github.io/blog/2016/vim-toggle-movement/
function! ToggleMovement(firstOp, secondOp, ...) abort
    let g:toggle_movement = get(g:, "toggle_movement", [1, a:firstOp])
    if g:toggle_movement[1] != a:firstOp
        let g:toggle_movement = [1, a:firstOp]
    endif

    let try_num = 1
    while try_num <= a:0 + 2
        let try_num = try_num + 1
        if g:toggle_movement[0] == 1
            let l:command = a:firstOp
        elseif g:toggle_movement[0] == 2
            let l:command = a:secondOp
        elseif a:0 == 1 && g:toggle_movement[0] == 3
            let l:command = a:1
        else
            echom "Something went wrong... (g:toggle_movement = " . string(g:toggle_movement) . ")"
            return
        endif

        if (a:0 == 0 && g:toggle_movement[0] == 2) || (a:0 == 1 && g:toggle_movement[0] == 3)
            let g:toggle_movement[0] = 1
        else
            let g:toggle_movement[0] = g:toggle_movement[0] + 1
        endif

        let pos = getpos('.')
        execute "normal! " . l:command
        if pos != getpos('.')
            break
        endif
    endwhile
endfunction
nnoremap <silent> ^ :call ToggleMovement('^', '$' ,'0')<cr>
nnoremap <silent> 0 :call ToggleMovement('0' ,'^', '$')<cr>
nnoremap <silent> $ :call ToggleMovement('$', '0', '^')<cr>
nnoremap <silent> ; :call ToggleMovement(';', ',')<cr>
nnoremap <silent> , :call ToggleMovement(',', ';')<cr>
nnoremap <silent> L :call ToggleMovement('L', 'M', 'H')<cr>
nnoremap <silent> M :call ToggleMovement('M', 'H', 'L')<cr>
nnoremap <silent> H :call ToggleMovement('H', 'L', 'M')<cr>
nnoremap <silent> G :call ToggleMovement('G', 'gg')<cr>
nnoremap <silent> gg :call ToggleMovement('gg', 'G')<cr>
```

## Installing via flatpack

    sudo flatpak install flathub io.neovim.nvim
    flatpak run io.neovim.nvim

## neovim --servername support

    " using vim-plug install neovim-remote

    Plug 'mhinz/neovim-remote'

    sudo pip3 install neovim-remote

    NVIM_LISTEN_ADDRESS=/tmp/nvimsocket nvr file1 file2
    nvr --servername /tmp/nvimsocket file1 file2

    nvr --remote file1 file2
    nvr --remote file1 $MYVIMRC

    nvr zsync.md
    nvr --remote test.txt && exit

    # Send keys to the current buffer:
    nvr --remote-send 'iabc<esc>'
    # Enter insert mode, insert 'abc', and go back to normal mode again.

    # Evaluate any VimL expression, e.g. get the current buffer:
    nvr --remote-expr 'bufname("")'
    README.md

    Open files always in the same nvim no matter which terminal you're in.

    If you just run "nvr -s", a new nvim process will start and set its address
    to /tmp/nvimsocket automatically.

    Now, no matter in which terminal you are, nvr file will always work on
    that nvim process. That is akin to emacsclient from Emacs.

## Usando nvr em conjunto com o fasd

Defina um alias para user o comando 'v'

```markdown
    alias v='f -e nvr -s'
    nvr `f dicasvim`
```

NOTE: $MYVIMRC is settled on my .zshenv

## Instant markdow preview
+ Plug 'suan/vim-instant-markdown', {'for': 'markdown'}

    [sudo] npm -g install instant-markdown-d

## open nvim from st (simple terminal)

    st -e nvim

## The emmet plugin for html
+ https://medium.com/vim-drops/be-a-html-ninja-with-emmet-for-vim-feee15447ef1

    table>tr*2>td*5

## inserting special chars in html
+ https://vim.fandom.com/wiki/HTML_entities

``` vim
" Escape/unescape & < > HTML entities in range (default current line).
function! HtmlEntities(line1, line2, action)
  let search = @/
  let range = 'silent ' . a:line1 . ',' . a:line2
  if a:action == 0  " must convert &amp; last
    execute range . 'sno/&lt;/</eg'
    execute range . 'sno/&gt;/>/eg'
    execute range . 'sno/&amp;/&/eg'
  else              " must convert & first
    execute range . 'sno/&/&amp;/eg'
    execute range . 'sno/</&lt;/eg'
    execute range . 'sno/>/&gt;/eg'
  endif
  nohl
  let @/ = search
endfunction
command! -range -nargs=1 Entities call HtmlEntities(<line1>, <line2>, <args>)
noremap <silent> <Leader>h :Entities 0<CR>
noremap <silent> <Leader>H :Entities 1<CR>


" Unescape lines 10 to 20 inclusive.
:10,20Entities 0

" Escape all lines.
:%Entities 1
```

## Find the last occurrence of one word

    :1?xxx

or

    :\vxxx\ze((xxx)@!\_.)*%$

## search a pattern only in a line range
+ https://jdhao.github.io/2019/11/11/nifty_nvim_techniques_s5/

    /\%>10l\%<20lsearch

## Creating icon for nvim
+ https://paulherron.com/blog/open_file_current_neovim_window

``` sh
[Desktop Entry]
Name=Neovim
GenericName=Text Editor
TryExec=nvim
Exec=nvim %F
Terminal=true
Type=Application
Keywords=Text;editor;
Icon=nvim
Categories=Utility;TextEditor;
StartupNotify=false
MimeType=text/english;text/plain;text/x-makefile;text/x-c++hdr;text/x-c++src;text/x-chdr;text/x-csrc;text/x-java;text/x-moc;text/x-pascal;text/x-tcl;text/x-tex;application/x-shellscript;text/x-c;text/x-c++;
```

## increasing numbers (disable no octal)

If you don't want to increment numbers in octal notation,

    :set nrformats-=octal.

<!--
vim: ft=markdown et sw=4 ts=4 cole=0 spell:
-->
