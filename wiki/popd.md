``` txt
file: popd.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```



acessa o último diretório colocado na pilha de acesso através
do comando [[pushd]].
