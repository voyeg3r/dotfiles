```
/home/sergio/.dotfiles/wiki/prettier.md
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

Prettier is "an opinionated code formatter". Prettier will format the style of your code per your configurations.

- https://www.freecodecamp.org/news/dont-just-lint-your-code-fix-it-with-prettier/

First, we will need to install the necessary packages. In some cases "prettier" does not appears
on the path, so I think it is better to install it globally.

    #yarn add prettier prettier-eslint prettier-eslint-cli -D
    doas npm install -g prettier

```lua
    -- use("p00f/nvim-ts-rainbow")
    use({
      "prettier/vim-prettier",
      run = "yarn install",
      ft = { "javascript", "typescript", "css", "less", "scss", "graphql", "markdown", "vue", "html" },
    })
```
