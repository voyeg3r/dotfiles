``` txt
file: xsel.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

Referências: http://www.dicas-l.com.br/arquivo/copiando_textos_no_terminal.php

Opções do Xsel

    Usando a opção -i o texto copiado irá para as 3 áreas
    Usendo a opção -b o texto é copiado para o modo gráfico

---------------------------------
Colando um comando dentro do gvim
---------------------------------

    ls -l | xsel -bi

	Chame o gvim e use

		"+p

