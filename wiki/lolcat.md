``` txt
File: lolcat.md
Author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
Date: nov 22, 2020 - 05:53
tags: [tags]
```

#  lolcat.md intro:
What is lolcat?

Lolcat is an utility for Linux, BSD and OSX which concatenates like similar to
cat command and adds rainbow coloring to it. Lolcat is primarily used for
rainbow coloring of text in Linux Terminal.

lolcat is based on ruby, so:

    gem install lolcat

## How to use it?

    ps aux | lolcat

