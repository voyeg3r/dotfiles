``` txt
file: artigos.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

# Artigos

+ [apropagandaideologicadopig](apropagandaideologicadopig.md)
+ [vidaaposavida](vidaaposavida.md)
+ [oparadoxopython](oparadoxopython.md)
+ [pythoneocaminhodomeio](pythoneocaminhodomeio.md)
+ [dicatedtalks](dicatedtalks.md)
+ [thefreesoftwareecosystem](thefreesoftwareecosystem.md)
+ [howtomakequestionsinenglish](howtomakequestionsinenglish.md)
+ [aindustriapornografica](aindustriapornografica.md)
+ [howlearnlanguagesfast](howlearnlanguagesfast)
+ [resposta ao sergio reis](resposta-ao-sergio-reis.md)
+ [O Bom Samaritano](O-Bom-Samaritano.md)
+ [Palavras de multiplo sentido](Palavras-de-multiplo-sentido.md)
+ [milk nao e somente leite](milk-nao-e-somente-leite)
+ [useless use of cat](useless-use-of-cat)

## Articles about vim
+ [introducao ao vim](introducao-ao-vim)
+ [how-to-copy-paste-on-vim](how-to-copy-paste-on-vim.md)
+ [youdontgrokvi](youdontgrokvi.md)
