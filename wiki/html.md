``` txt
File: html.md
Author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
Date: jan 31, 2021 - 14:03
tags: [tags]
```

## emdash is not emdash
+ https://prowebtype.com/discerning-typographic-details/

    &emdash;
    &lsquo;

    ‘ single opening 	Opt + ] 	        Alt + 0145 	&lsquo; or &#8216;
    ’ single closing 	Opt + Shift + ] 	Alt + 0146 	&rsquo; or &#8217;
    “ double opening 	Opt + [ 	        Alt + 0147 	&ldquo; or &#8220;
    ” double closing 	Opt + Shift + [ 	Alt + 0148 	&rdquo; or &#8221;
     “”
     non-separable space: &nbsp;

     arrow →  &rarr;
