``` txt
file: whatis.md
author: Sergio Araujo
Last Change:  Sat, 07 May 2022 - 21:00:06
```


# Intro
Exibe informações básicas sobre um comando, por exemplo

    whatis find
    find (1)             - search for files in a directory hierarchy
    find (1p)            - find files
    find (n)             - search for classes and objects

## Using whatis and whereis Commands to Know the Manual’s Sections

    whatis passwd
    passwd(1) - change user password
    passwd, openssl-passwd(1, 1ssl) - compute password hashes
    passwd(5) - the password file
