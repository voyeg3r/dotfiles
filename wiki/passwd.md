``` txt
file: passwd.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```


# Define senhas

Se o comando passwd for usado sem argumentos, ou seja, sem indicar
o usuário o sistema considera que se deseja modificar a senha do
usuário corrente
