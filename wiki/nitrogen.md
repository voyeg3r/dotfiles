``` txt
file: nitrogen.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39

# Nitrogen

Nitrogen is a fast and lightweight desktop background browser and setter for X windows.

In my case I use it to set my [i3](i3.md) background
