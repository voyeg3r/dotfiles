```txt
File: /home/sergio/.dotfiles/wiki/xinput.md
Last Change:  Fri, 20 May 2022 - 11:00:40
tags: [x11, utils, tools]
```

## Intro:
+ http://xahlee.info/linux/linux_xvkbd_tutorial.html

xinput -  Utility to configure and test X input devices

In terminal, type xinput --list to list devices.

    xinput --list | grep 'id='
