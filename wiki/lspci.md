``` txt
file: lspci.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

O comando lspci lista os dispositivos como video audio etc

Combinado com o grep podemos listar apenas a marca da placa de vídeo

lspci | grep VGA
