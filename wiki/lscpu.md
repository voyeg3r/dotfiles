``` txt
file: lscpu.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

# lscpu.md - dom 12 nov 2017 20:36:23 -03

Gives complete infromation about processor

## How many cores do I have on my processor

    sed -n '2p' <(lscpu) | pbcopy

## tags: hardware, cpu, processors
