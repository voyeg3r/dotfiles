``` txt
file: deepin.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

# deepin.md
+ https://www.youtube.com/watch?v=f3iphFPU_fI
+ https://www.youtube.com/watch?v=q_DK40QNU98

## Atalhos de tecla
+ https://www.youtube.com/watch?v=MPHLa2QR-Uw

	Super + w ............. spreed windows
	Super + a ............. show all opened apps
	Super + s ............. show virtual desktops
	Super + l ............. lock screen
	Super + e ............. File manager
	Super + + ............. zoom in
	Shift + Super + arrow . change app to another virtual desktop
	Ctrl + Alt + a ........ screenshot region

## Deeping terminal shortcuts

    Ctrl + Shift + h .......... dividir terminal horizontalmente

