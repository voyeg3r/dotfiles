``` txt
file: soundconverter.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```


# soundconverter
+ https://ubuntuforums.org/showthread.php?t=1835496

    soundconverter -b -m audio/mpeg -q -s .mp3 "$f.wav"

    -b ............. batch mode
    -m ............. mime-type for output file
    -q ............. quite
    -s ............. extension for output file (does not affect output mime-type)

