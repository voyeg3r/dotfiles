``` txt
file: medium.md
author: Sergio Araujo
Last Change:  Sat, 16 Jul 2022 - 20:06:58
```

# Medium.md
+ https://help.medium.com/hc/en-us/sections/115001484747-Editing-formatting

Tips on how format medium.org articles

    Shortcut   | Result
    -----------|-------------
    Ctrl-b     | Bold
    Ctrl-Alt-2 | Title2
    Ctrl-Alt-6 | Code blocks

## How to embed github gists on medium?

Select "Share" instead of embed, copy the url and paste on Medium tapping
Enter twice.

