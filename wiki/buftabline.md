``` txt
File: buftabline.md
Author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
tags: [nvim, plugins]
```

#  buftabline.md intro:

    Plug 'ap/vim-buftabline'
    highlight BufTabLineCurrent ctermfg=109
    highlight BufTabLineCurrent ctermbg=green
    highlight BufTabLineActive ctermbg=white
    highlight BufTabLineHidden ctermbg=darkgrey
    highlight BufTabLineFill ctermbg=grey

