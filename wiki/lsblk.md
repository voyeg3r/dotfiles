``` txt
file: lsblk.md
author: Sergio Araujo
Last Change: Mon, 01 Aug 2022 14:31:53
tags: [usb, stick, livecd, tools]
```

## How do I know the device path to an USB-stick?
+ https://askubuntu.com/a/311775/3798

  lsblk -l

Mostra a identificação única de cada dispositivo de tal modo
que se por exemplo você montar um pendrive pela sua identificação
única UUID, independente da entrada ele será identificado
igualmente.

outro comando relacionado é o "blkid"
