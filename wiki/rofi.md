```txt
File: /home/sergio/.dotfiles/wiki/rofi.md
Last Change: Mon, 08 Aug 2022 12:34:20
tags: [utils, rofi, bspwm, tools]
```

## Intro:
+ https://github.com/maledorak/dmenu-hotkeys
+ https://github.com/svenstaro/rofi-calc
+ pipx install ddgr
+ https://github.com/gmagno/rofi-ddgr
+ https://terminalroot.com.br/2020/05/como-customizar-o-rofi-like-a-foss.html

    pipx install dmenu-hotkeys
    pipx install ddgr

## Choose rofi theme:

    rofi-theme-selector

### Installing new rofi themes:

```sh
git clone https://github.com/lr-tech/rofi-themes-collection.git
cd rofi-themes-collection
mkdir -p ~/.local/share/rofi/themes/
```


## Set rofi as default in clipmenu:
+ https://github.com/cdown/clipmenu

     $CM_LAUNCHER
