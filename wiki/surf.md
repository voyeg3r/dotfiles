```txt
File: /home/sergio/.dotfiles/wiki/surf.md
Last Change:  Fri, 27 May 2022 - 12:45:52
tags: [browser, web, suckless]
```

    surf url .................... open url
    ctrl + g .................... dmenu for opening urls
    ctrl + p .................... Ctrl+p: Go to URL in cut and paste clipboard.
    Ctrl+y: ..................... Put current website URL into cut and paste clipboard.

## opening suf with tabs:

    tabbed surf -pe

