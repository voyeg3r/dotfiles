``` txt
file: cpuinfo.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

# quantos núcleos tem o meu processador?

    grep -c ^processor /proc/cpuinfo

## vendor

    cat /proc/cpuinfo | grep vendor | uniq
    vendor_id       : GenuineIntel

Its an Intel processor. Next find the model name that can be used to lookup
the exact specifications online on Intel's website.

    cat /proc/cpuinfo | grep 'model name' | uniq
    model name      : Intel(R) Core(TM) i3-2328M CPU @ 2.20GHz

