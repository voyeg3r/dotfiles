---
File: glow.md
Author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
tags: [tags]
---

##  Render markdown on the CLI, with pizzazz
+ https://itsfoss.com/glow-cli-tool-markdown/

You can use the pager option to display the rendered text in pager mode (like how the less command shows the text without cluttering the screen).

    glow -p markdown_file
