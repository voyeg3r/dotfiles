``` txt
file: englishtexts.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

[fundamentals](fundamentals.md)
[towraparaundmyfinger](towraparaundmyfinger.md)
[learnlikeachildren](learnlikeachildren.md)
[quantumleapenglish](quantumleapenglish.md)
[colaboracaoemmassanainternet](colaboracaoemmassanainternet.md)
[dobblingbrainpower](dobblingbrainpower.md)
[generalenglishtips](generalenglishtips.md)
[limping](limping.md)
[ebola](ebola.md)
[story](story.md)
[plano-de-aula](plano-de-aula.md)

