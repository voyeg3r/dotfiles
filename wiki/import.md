``` txt
file: import.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```

# Introdução
Para capturar a imagem de um pedaço da tela faça:

    import imagem.png
