``` txt
file: shutdown.md
author: Sergio Araujo
Last Change:  Tue, 19 Jul 2022 - 11:37:18
tags: [admin]
```

## Introdução

## reiniciar agora

    shutdown -r 0

##  desligar as 17:00

    shutdown -h 17:00

## desligar em 30 segundos

    shutdown -h +30

## Desligar máquina windows remotamente

```bash
net rpc shutdown -I ipAddressOfWindowsPC -U username%password
net rpc shutdown -r : reboot the Windows machine
net rpc abortshutdown : abort shutdown of the Windows machine
```

