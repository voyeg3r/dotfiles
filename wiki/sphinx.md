``` txt
file: sphinx.md
author: Sergio Araujo
Last Change:  Tue, 26 Apr 2022 - 13:07:39
```


# Gerador de documentação do python
+ [slide de exemplo](http://slides.com/lucassimonrodriguesmagalhaes/sphinx/fullscreen#/)
## O que é

Sphinx é uma ferramenta para criar uma documentação de maneira fácil,
inteligente e bonita.

## Como instalar?

    pip install sphinx

## Inicializando a doc

     mkdir docs

     cd docs

     sphinx-quickstart

## Compilando Para HTML

     make html

## EXIBINDO NO BROWSER

     python -m SimpleHTTPServer 9000


