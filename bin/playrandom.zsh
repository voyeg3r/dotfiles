#!/usr/bin/env zsh
#
#          file: playrandom.zsh
#   Last Change: Sun, 22 May 2022 20:20
#       Purpose: play audio randomically
#
# atribuindo valores a um array
# src_files=(**/*.mp3)
#
# Acessando valores em um array
# echo $arr[1]
#
# So to see how many elements are in the arr array, we can do the following:
# echo ${#arr}
#
# random array element
# FILES=( .../files/* )
# feh $FILES[$RANDOM%$#FILES+1]
#
# file=$src_files[RANDOM%$#src_files+1]

# depedencies: which programs do you need to run this script
NEEDED_COMMANDS="mpg123"
missing_counter=0
for needed_command in $NEEDED_COMMANDS; do
    if ! hash "$needed_command" >/dev/null 2>&1; then
        printf "Command not found in PATH: %s\n" "$needed_command" >&2
        ((missing_counter++))
    fi
done
if ((missing_counter > 0)); then
    printf "Minimum %d commands are missing in PATH, aborting" "$missing_counter" >&2
    exit 1
fi

# performs a recursive search for mp3 files
src_files=(**/*.mp3)

# ask(){
#     echo "\n\tPress ENTER to continue\n\n\t[X/x] to EXIT\n\n\t[R/r] to REPEAT\n\n\
#         [C/c] to copy to ~/tmp/flaschards " && read -r response
# }

ask(){
cat <<-EOF

        [Enter]  to continue

        [X/x]    to Exit

        [R/r]    to repeat

        [C/c]    to copy to ~/tmp/flashcards
EOF
}

clear
while true; do

    clear
    echo
    selection=$src_files[RANDOM%$#src_files+1]
    #selection=$(find -type f -iname "*.mp3" | shuf -n 1)

    echo "\tPLAYING\t ${selection:t}"
    mpg123 -q $selection

    ask && read -r response

    if [[ "${response:l}" = x ]]; then
        clear
        exit 1
    fi

    if [[ "${response:l}" = c ]]; then
        if [[ ! -d "~/tmp/flashcrds" ]]; then
            mkdir -p ~/tmp/flashcards
        fi

        cp -f $selection ~/tmp/flashcards

    fi

    if [[ "${response:l}" = r ]]; then
        clear
        echo
        echo "\tREPEATING ${selection:t}"
        mpg123 -q "$selection"

	fi

done
